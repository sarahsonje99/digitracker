import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions, Response, ResponseContentType } from '@angular/http';

@Injectable({ providedIn: 'root' })
export class FormService{
    permitMetaData: any;
    constructor(private http:Http) {

    }
    // Returns all the form submitted by Admin
    getDependency() {

        let headers = new Headers({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true'
        });
        let options = new RequestOptions({ headers: headers });
        return this.http.get('assets/data/permitMetadata.json', options)
    }
}