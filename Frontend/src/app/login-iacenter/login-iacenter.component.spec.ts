import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginIacenterComponent } from './login-iacenter.component';

describe('LoginIacenterComponent', () => {
  let component: LoginIacenterComponent;
  let fixture: ComponentFixture<LoginIacenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginIacenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginIacenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
