import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { UserService } from '../shared/service/user.service';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-login-iacenter',
  templateUrl: './login-iacenter.component.html',
  styleUrls: ['./login-iacenter.component.css']
})
export class LoginIacenterComponent implements OnInit {

  username= new FormControl('');
  iacentername = new FormControl('');
  password = new FormControl('');

  constructor(private http: Http, private router: Router,private userservice:UserService,private sstorage:SessionStorageService) { }

  ngOnInit() {
  }

  invalidDetails() {
    // let length = this.password.value.length;
    // let islower = /^[a-z]*$/.test(this.password.value);
    // let isUpper = /^[A-Z]*$/.test(this.password.value);
    // if(length < 8)
    //   return false;
    // else if(this.password.value.search(/\d/) == -1)
    //   return false;
    // else if (this.password.value.search(/[a-z]/) == -1) 
    //   return false;
    // else if (this.password.value.search(/[A-Z]/) == -1)
    //   return false;
    // else if (this.password.value.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1)
    //     return false;
    // else
      return true;
  }

  loginIACenter() {
    // Rest API call for login of Issuing Authority
    this.http.post('http://localhost:4100/api/loginiacenter', {
      username: this.username.value,
      iacentername: this.iacentername.value,
      password: this.password.value
    }).subscribe((data:any) => {
      console.log(data)
      let data1 = JSON.parse(data._body)
      if(data1.auth == true) {
        console.log("LOGIN SUCCESSFUL");
        this.sstorage.store('iacentre',this.iacentername.value);
        this.sstorage.store("loggedInIACentre",true);
        this.userservice.loggedIA.emit(true);
        this.router.navigate(['forms-rto']);
      }
    });
  }
}
