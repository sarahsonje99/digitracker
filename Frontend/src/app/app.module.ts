import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
import { HttpModule, Headers } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './register/register.component';
import { FormgenComponent } from './formgen/formgen.component';
import { UserpermitComponent } from './userpermit/userpermit.component';
import { PaymentRequestComponent } from './payment-request/payment-request.component'
import { LoginUserComponent } from './login-user/login-user.component';
import { FormsRtoComponent } from './forms-rto/forms-rto.component';
import { FileuploadComponent } from './fileupload/fileupload.component';

import { UserService } from './shared/service/user.service';
import { FormService } from './shared/service/form.service';
import { WebsocketService } from './websocket.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { MyMaterialModule } from  './material.module';
import { environment } from '../environments/environment';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {Ng2Webstorage, WebStorageService} from 'ngx-webstorage';
import { NavbarComponent } from './navbar/navbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatStepperModule } from '@angular/material/stepper';
// import { NgxPayPalModule } from 'ngx-paypal';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
// import { NavUserComponent } from './nav-user/nav-user.component';
import { PaySuccessComponent } from './pay-success/pay-success.component';
import { FormrenComponent } from './formren/formren.component';
import { AplToVerComponent } from './apl-to-ver/apl-to-ver.component';
import { VerToValComponent } from './ver-to-val/ver-to-val.component';
import { ValToIssComponent } from './val-to-iss/val-to-iss.component';
import { LoginIaComponent } from './login-ia/login-ia.component';
import { LoginIacenterComponent } from './login-iacenter/login-iacenter.component';
import { HomeComponent } from './home/home.component';
import { Navbar2Component } from './navbar2/navbar2.component';
import { ViolationComponent } from './violation/violation.component';
import { DatePipe } from '@angular/common';
import { PsychComponent } from './psych/psych.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    FormgenComponent,
    UserpermitComponent,
    PaymentRequestComponent,
    LoginUserComponent,
    FileSelectDirective,
    FileuploadComponent,
    FormsRtoComponent,
    NavbarComponent,
    FormrenComponent,
    AplToVerComponent,
    VerToValComponent,
    ValToIssComponent,
    // NavUserComponent,
    PaySuccessComponent,
    LoginIaComponent,
    LoginIacenterComponent,
    HomeComponent,
    Navbar2Component,
    ViolationComponent,
    PsychComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MyMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    FlexLayoutModule,
    HttpModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    NgbModule.forRoot()
    // NgxPayPalModule
  ],
  providers: [UserService, FormService, WebsocketService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
