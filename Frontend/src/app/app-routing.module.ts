import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { FormgenComponent } from './formgen/formgen.component';
import { UserpermitComponent } from './userpermit/userpermit.component';
import { PaymentRequestComponent } from './payment-request/payment-request.component';
import { LoginUserComponent } from './login-user/login-user.component';
import { FileuploadComponent } from './fileupload/fileupload.component';
import { FormsRtoComponent } from './forms-rto/forms-rto.component';
import { PaySuccessComponent } from './pay-success/pay-success.component';
import { FormrenComponent } from './formren/formren.component';
import { AplToVerComponent } from './apl-to-ver/apl-to-ver.component';
import { VerToValComponent } from './ver-to-val/ver-to-val.component';
import { LoginIacenterComponent } from './login-iacenter/login-iacenter.component';
import { LoginIaComponent } from './login-ia/login-ia.component';
import { ValToIssComponent } from './val-to-iss/val-to-iss.component';
import { HomeComponent } from './home/home.component';
import { ViolationComponent } from './violation/violation.component';
import {PsychComponent} from './psych/psych.component';

const approutes:Routes=[
    // { path: '', redirectTo: '/', pathMatch: 'full' },
    { path:'',component:HomeComponent},
    { path: 'login-ia', component: LoginIaComponent },
    { path: 'login-iacenter', component: LoginIacenterComponent },
    { path: 'login', component: LoginUserComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'formgen', component: FormgenComponent },
    { path: 'mypermit', component: UserpermitComponent },
    { path: 'payment', component: PaymentRequestComponent },
    { path: 'file-upload', component: FileuploadComponent },
    { path: 'forms-rto', component: FormsRtoComponent },
    { path: 'formren', component: FormrenComponent },
    { path: 'apl-to-ver', component: AplToVerComponent},
    { path: 'ver-to-val', component: VerToValComponent},
    { path: 'val-to-iss', component: ValToIssComponent},
    { path: 'pay-success', component: PaySuccessComponent },
    { path: 'violation', component: ViolationComponent },
    { path: 'psych', component:PsychComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(approutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
