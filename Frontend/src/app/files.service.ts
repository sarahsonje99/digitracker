import { Injectable } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  constructor(private http: Http) { }

  downloadPDF(filename, filetype): any {
    return this.http.get('http://127.0.0.1:4100/api/file/' + filename, { responseType: ResponseContentType.Blob });
  }

  showFileNames() {
    return this.http.get('http://127.0.0.1:4100/api/files');
  }
}
