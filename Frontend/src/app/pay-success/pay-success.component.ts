import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pay-success',
  templateUrl: './pay-success.component.html',
  styleUrls: ['./pay-success.component.css']
})
export class PaySuccessComponent implements OnInit {

  data:any;

  paymentId: string;
  PayerID: string;

  constructor(private http: Http, private route: ActivatedRoute) { 
    this.route.queryParams.subscribe(params => {
      this.paymentId = params['paymentId'];
      this.PayerID = params['PayerID'];
      console.log(this.paymentId);
      console.log(this.PayerID);
      this.http.post("http://localhost:4100/api/pay-success", {
        "paymentId": this.paymentId,
        "PayerID": this.PayerID
      })
      .subscribe((data:any) => {
        this.data = data;
        console.log(this.data._body);
      });
    });
  }
  ngOnInit() {
  }
}
