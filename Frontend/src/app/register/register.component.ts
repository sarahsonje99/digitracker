import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from  '../auth/auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  aadhaarBoolean: boolean;
  mobileBoolean: boolean;
  otpMobileSent: boolean;
  emailBoolean: boolean;
  otpEmailSent: boolean;
  detailsBoolean: boolean;

  requestId: any;
  aadhaarID = new FormControl('');
  mobile = new FormControl('');
  otpMobile = new FormControl('');
  email = new FormControl('');
  otpEmail = new FormControl('');
  otpObtained: any;

  class = 'digitracker.network.Citizen';
  firstname = new FormControl('');
  middlename = new FormControl('');
  lastname = new FormControl('');
  dob = new FormControl('');
  // password = new FormControl('');
  // rPassword = new FormControl('');

  constructor(private router: Router, private http: Http, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.aadhaarBoolean = true;
    this.mobileBoolean = false;
    this.otpMobileSent = false;
    this.emailBoolean = false;
    this.otpEmailSent = false;
    this.detailsBoolean = false;

    console.log(this.mobile);
  }

  invalidAadhaar() {
    let length = this.aadhaarID.value.length;
    var isnum = /^\d+$/.test(this.aadhaarID.value);
    if(length != 12)
      return false;
    else if(!isnum)
      return false
    else
      return true;
  }

  aadhaar(){
    // Send OTP
    this.aadhaarBoolean = false;
    this.mobileBoolean = true;
  }

  invalidMobile() {
    let length = this.mobile.value.length;
    var isnum = /^\d+$/.test(this.mobile.value);
    if(length != 10)
      return false;
    else if(!isnum)
      return false
    else
      return true;
  }

  sendMobileOtp() {
    let jsonData = ({"number": '91'+this.mobile.value})
    console.log(jsonData)
    // this.http.post("http://192.168.1.2:4100/api/otp",({
    this.http.post("http://localhost:4100/api/otp",({
      number: '91'+this.mobile.value
    }))
    .subscribe((data: any) =>{
      let data1 = JSON.parse(data._body);
      this.requestId = data1.requestId
      console.log(this.requestId);
      this.otpMobileSent = true;
    },error=>{
        console.log(error);
    });
  }

  invalidOtp() {
    var isnum = /^\d+$/.test(this.otpMobile.value);
    if(this.otpMobile.value.length != 4)
      return false;
    else if(!isnum)
      return false;
    else 
      return true;
  }

  verifyMobileOtp() {
    // this.http.post("http://192.168.1.2:4100/api/verify-otp",{
    this.http.post("http://localhost:4100/api/verify-otp",{
      pin: this.otpMobile.value,
      requestId: this.requestId
    })
    .subscribe((data: any) =>{
      let data1 = JSON.parse(data._body);
      let message = data1.message;
      if(message === 'Account verified!') {
        this.mobileBoolean = false;
        this.emailBoolean = true;
      }
      console.log(this.requestId)
    },error=>{
        console.log(error);
    });
  }

  sendEmailOtp() {
    // this.http.post('http://192.168.1.2:4100/api/email-verify',{
    this.http.post('http://localhost:4100/api/email-verify',{
      "email": this.email.value
    }).subscribe((data:any)=>{
      let jsonData = data;
      console.log(data);
      this.otpObtained = jsonData._body;
      console.log(this.otpObtained);
      this.otpEmailSent = true;
    });
  }

  verifyEmailOtp() {
    if(this.otpObtained === this.otpEmail.value){
      this.emailBoolean = false;
      this.detailsBoolean = true;
    }
  }

  invalidDetails() {
    if(this.firstname.value.search(/\d/) != -1)
      return false;
    if(this.middlename.value.search(/\d/) != -1)
      return false;
    if(this.lastname.value.search(/\d/) != -1)
      return false;
    else
      return true;
  }

  submitDetails() {
    // this.http.post('http://192.168.1.2:4100/api/register', {
      this.http.post('http://localhost:4100/api/register', {
      "$class": this.class,
      "aadhaarId": this.aadhaarID.value,
      "dob": this.dob.value,
      "firstName": this.firstname.value,
      "middleName": this.middlename.value,
      "lastName": this.lastname.value,
      "phoneNo": this.mobile.value,
      "emailId": this.email.value
    }).subscribe((data: any) => {
      let data1 = JSON.parse(data._body);
      console.log(data1);
      if(data1 === true){
        console.log("SUCCESS")
        this.router.navigate(['/login']);
      } else {
        this.router.navigate(['/register']);
      }
    });
  }
}
