import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from '../shared/service/user.service';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { userInfo } from 'os';
import { LoginUserComponent } from '../login-user/login-user.component';
import { RegisterComponent } from '../register/register.component';

@Component({
    selector: 'app-navbar2',
    templateUrl: './navbar2.component.html',
    styleUrls: ['./navbar2.component.css'],
    providers: [SessionStorageService]
})
export class Navbar2Component implements OnInit {
    permitNames:{"key":string,"value":string[]}[]=[];

    showall:boolean=false;
    loggedUser:boolean=false;
    loggedRto:boolean=false;

    mypermitlist=[];
    permitsall=[];

    constructor(public dialog1: MatDialog,public dialog2: MatDialog,private sstorage:SessionStorageService,private userservice:UserService,private router:Router,private breakpointObserver: BreakpointObserver) {
        this.userservice.reloadnav.subscribe(data=>{
        if(data==true) {
            this.ngOnInit();
        }
        });
    this.userservice.logging.subscribe(data=>{
      if(data==true)
      {
        this.loggedUser=true;
      }
      if(data==false)
      {
        this.loggedUser=false;
      }
    });

  }
  ngOnInit()
  {
    if(this.sstorage.retrieve('loggedIn')==true)
    {
      this.loggedUser=true;
    }
    this.mypermitlist=[];
      this.userservice.listOfUserPermits(this.sstorage.retrieve("userId")).subscribe(data=>{
        this.mypermitlist.push(data);
      });
      this.permitsall=[];
      this.userservice.allPermitsMetaData().subscribe(data=>{
        this.permitsall.push(data);
       console.log(this.permitsall);
       this.getPermitNames();
      });
  }

  openLoginDialog(): void {
    const dialogRef = this.dialog1.open(LoginUserComponent, {
      width: '400px',
      height:'300px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openRegisterDialog(): void {
    const dialogRef = this.dialog2.open(RegisterComponent, {
      width: '400px',
      height:'300px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getPermitNames()
  {
    this.permitNames=[];
    console.log(this.permitsall);
    var ianames=[];
    var unique = {};
    var distinct = [];
        for( var i in this.permitsall ){
         if( typeof(unique[this.permitsall[i].IAName]) == "undefined"){
          distinct.push(this.permitsall[i].IAName);
         }
         unique[this.permitsall[i].IAName] = 0;
        }
    for(var i in distinct)
    {
      var arr=[];
      for(var j in this.permitsall)
      {
        
        if(distinct[i]==this.permitsall[j].IAName)
        {
            arr.push(this.permitsall[j].permitName);
        }
      }
      this.permitNames.push({"key":distinct[i],"value":arr});
    }
    this.showall=true;
    console.log(this.permitNames);
  }

  goToMyPermit(selectedPermit){
    console.log(selectedPermit);
    this.sstorage.clear('selectedPermit');
    this.sstorage.store('selectedPermit',JSON.stringify(selectedPermit));
    this.userservice.openUserPermit.emit(true);
      this.router.navigate(['mypermit']);
      //this.router.navigate(['mypermit']);
  }

  goToAllPermit(permit,chosenIA)
  {
    console.log(this.sstorage.retrieve("userId"));
    var x;
    var y;    //for marking which form it is if it exist in userpermits
    var permitexist=false;
    var selectedPermit;
    console.log(permit);
    for (x in this.mypermitlist)
    {
      console.log(this.mypermitlist[x].permitName);
      if(permit==this.mypermitlist[x].permitName)
      {
        selectedPermit=this.mypermitlist[x];
        //console.log(permit.permitName);
        y=x;
        permitexist=true;
        break;
      }
    }
    if(permitexist==false)
    {
     
      this.sstorage.store("permitMetadataName",permit);
      this.sstorage.store("chosenIA",chosenIA);
      // console.log(JSON.stringify(permit));
      
      this.router.navigate(['formren']);
      this.userservice.openPermitMetaData.emit(true);
    }else{
      console.log(selectedPermit);
      this.goToMyPermit(selectedPermit);
      // console.log(selectedPermit);
      // this.sstorage.clear('selectedPermit');
      // this.sstorage.store('selectedPermit',JSON.stringify(selectedPermit));
      // this.userservice.openUserPermit.emit(true);
      // this.router.navigate(['mypermit']);
    }
  }

  logOut()
  {
    console.log("log");
    this.userservice.logging.emit(false);
    this.sstorage.clear('userId');
    this.sstorage.clear();
    console.log(this.sstorage.retrieve('permitMetadataName'));
    this.router.navigate(['']); 
  }


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches)
  );
}
