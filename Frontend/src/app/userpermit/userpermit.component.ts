import { Component, OnInit } from '@angular/core';
import { config, defaultI18n, defaultOptions } from "../formbuilder/config";
import { FormBuilderCreateor } from "../formbuilder/form-builder";
import I18N from "../formbuilder/mi18n";
import * as JQuery from 'jquery';
import 'jqueryui/jquery-ui.min.js';
import * as ffRender from "../formbuilder/formrender.js";
import { UserService } from '../shared/service/user.service';
import { SessionStorageService } from 'ngx-webstorage';

declare var jquery:any;
declare var $ :any;


@Component({
  selector: 'app-userpermit',
  templateUrl: './userpermit.component.html',
  styleUrls: ['./userpermit.component.css']
})

export class UserpermitComponent implements OnInit {
  selectedPermit;
  form=[];
  pbar;
  singleStepAnimation=1000;
  renderingWaitDelay = 200; 

  application:{"$class":string,"permitName":string,"aadhaarId":string,"timestamp":string}={"$class":"digitracker.network.GetPermitHistory","permitName":"","aadhaarId":"","timestamp":""};
  timeline=[];


  penalties=[];
  rejections=[];
  suspensions=[];
  revocations=[];
  
  constructor(private userservice:UserService,private sstorage:SessionStorageService) { 
    this.userservice.openUserPermit.subscribe(data=>{
      if(data==true)
      {
        // this.displayLifecycle();
        this.displayTimeline();
        this.displayForm();
       
        this.getPenalty();
        this.getSuspension();
        this.getRevocation();
        this.animation();
      }
    });
  }
  ngOnInit() {
    this.pbar={};
    this.displayTimeline();
    this.displayForm();
   
    this.getPenalty();
    this.getSuspension();
    this.getRevocation();
    this.animation();
    // this.initial(
    //   [ 'SUBMITTED',
    //   'VERIFIED',
    //   'VALIDATED',
    //   'ISSUED',
    //   'EXPIRED',

    // ],
    // 'VALIDATED',
    // 'progress-bar-wrapper'
    // );

  }

  displayTimeline(){
    //fetching permit lifecycle
		this.application.permitName=JSON.parse(this.sstorage.retrieve("selectedPermit")).permitName;
		this.application.aadhaarId=JSON.parse(this.sstorage.retrieve("selectedPermit")).aadhaarId;
		this.application.timestamp=new Date().toISOString();
		this.userservice.viewTimeline(JSON.stringify(this.application)).subscribe((data)=>{
		this.timeline=JSON.parse(data).eachStage;
			console.log(data);
		});
  }


// --------------------animation--------------------
animation(){
  // Vertical Timeline - by CodyHouse.co
function VerticalTimeline( element ) {
  this.element = element;
  this.blocks = this.element.getElementsByClassName("js-cd-block");
  this.images = this.element.getElementsByClassName("js-cd-img");
  this.contents = this.element.getElementsByClassName("js-cd-content");
  this.offset = 0.8;
  this.hideBlocks();
};

VerticalTimeline.prototype.hideBlocks = function() {
  //hide timeline blocks which are outside the viewport
  if ( !("classList" in document.documentElement )) {
    return;
  }
  var self = this;
  for( var i = 0; i < this.blocks.length; i++) {
    (function(i){
      if( self.blocks[i].getBoundingClientRect().top > window.innerHeight*self.offset ) {
        self.images[i].classList.add("cd-is-hidden"); 
        self.contents[i].classList.add("cd-is-hidden"); 
      }
    })(i);
  }
};

VerticalTimeline.prototype.showBlocks = function() {
  if ( !("classList" in document.documentElement ) ) {
    return;
  }
  var self = this;
  for( var i = 0; i < this.blocks.length; i++) {
    (function(i){
      if( self.contents[i].classList.contains("cd-is-hidden") && self.blocks[i].getBoundingClientRect().top <= window.innerHeight*self.offset ) {
        // add bounce-in animation
        self.images[i].classList.add("cd-timeline__img--bounce-in");
        self.contents[i].classList.add("cd-timeline__content--bounce-in");
        self.images[i].classList.remove("cd-is-hidden");
        self.contents[i].classList.remove("cd-is-hidden");
      }
    })(i);
  }
};

var verticalTimelines = document.getElementsByClassName("js-cd-timeline"),
  verticalTimelinesArray = [],
  scrolling = false;
if( verticalTimelines.length > 0 ) {
  for( var i = 0; i < verticalTimelines.length; i++) {
    (function(i){
      verticalTimelinesArray.push(new VerticalTimeline(verticalTimelines[i]));
    })(i);
  }

  //show timeline blocks on scrolling
  document.getElementsByClassName("mat-drawer-content")[0].addEventListener("scroll", function(event) {
    console.log("hi");
    if( !scrolling ) {
      scrolling = true;
      (!requestAnimationFrame) ? setTimeout(checkTimelineScroll, 250) : requestAnimationFrame(checkTimelineScroll);
    }
  });
}

function checkTimelineScroll() {
  console.log("hi");
  verticalTimelinesArray.forEach(function(timeline){
    timeline.showBlocks();
  });
  scrolling = false;
};
}
// --------------------animation--------------------
// ------------------------- for timeline -----------------
displayLifecycle()
{
  this.selectedPermit=JSON.parse(this.sstorage.retrieve("selectedPermit"));
  if(this.selectedPermit.isRejected == false)
  {
      this.initial([ 'APPLIED','VERIFIED','VALIDATED','ISSUED','EXPIRED'],
                  this.selectedPermit.stage,
                  'progress-bar-wrapper'
                  );
  }else{
    if(this.selectedPermit.stage=='VERIFIED')
    {
      this.initial(['APPLIED','REJECTED','VALIDATED','ISSUED','EXPIRED'],
    'REJECTED',
    'progress-bar-wrapper');
    }
    else if(this.selectedPermit.stage=='VALIDATED')
    {
      this.initial(['APPLIED','VERIFIED','REJECTED','ISSUED','EXPIRED'],
      'REJECTED',
      'progress-bar-wrapper');
    }
  
  }
}

    createElement = function (type, className, style, text) {
      var elem = document.createElement(type);
      elem.className = className;
      for (var prop in style) {
        elem.style[prop] = style[prop];
      }
      elem.innerHTML = text;
      return elem;
    }


    createStatusBar = function(stages, stageWidth, currentStageIndex) {
      var statusBar = this.createElement('div', 'status-bar', {width: 100 - stageWidth + '%'}, '');
      var currentStatus = this.createElement('div', 'current-status', {}, '');

      setTimeout(function() {
        currentStatus.style.width = (100 * currentStageIndex)/(stages.length - 1)+'%';
        currentStatus.style.transition = 'width '+(currentStageIndex * this.singleStepAnimation)+'ms linear';
      }, this.renderingWaitDelay);

      statusBar.appendChild(currentStatus);
      return statusBar;
    }



    createCheckPoints = function(stages, stageWidth, currentStageIndex) {
      var ul = this.createElement('ul', 'progress-bar', { }, '');
      var animationDelay = this.renderingWaitDelay;
      for (var index = 0; index < stages.length; index++) {
        var li = this.createElement('li', 'section', {width: stageWidth-0.4 + 'vw'}, stages[index]);
        if(currentStageIndex >= index) {
          setTimeout(function(li, currentStageIndex, index) {
            li.className+= (currentStageIndex > index)?' visited': ' visited current';
          }, animationDelay, li, currentStageIndex, index);
          animationDelay+= this.singleStepAnimation;
        }
        ul.appendChild(li);
      }
      return ul;
    }


     createHTML = function (wrapper, stages, currentStage) {
      var stageWidth = 100 / stages.length;
      var currentStageIndex = stages.indexOf(currentStage);

      //create status bar
     
        var statusBar = this.createStatusBar(stages, stageWidth, currentStageIndex);
        wrapper.appendChild(statusBar);  
      
      //create checkpoints
      var checkpoints = this.createCheckPoints(stages, stageWidth, currentStageIndex);
      wrapper.appendChild(checkpoints);

      return wrapper;
    }


    validateParameters = function(stages, currentStage, container) {
      if(!(typeof stages === 'object' && stages.length && typeof stages[0] === 'string')) {
        console.error('Expecting Array of strings for "stages" parameter.');
        return false;
      }
      if(typeof currentStage !== 'string') {
        console.error('Expecting string for "current stage" parameter.');
        return false;
      }
      if(typeof container !== 'string' && typeof container !== 'undefined') {
        console.error('Expecting string for "container" parameter.');
        return false;
      }
      return true;
    }

    initial(stages, currentStage, container) {
      if(this.validateParameters(stages, currentStage, container)) {
        var wrapper = document.getElementById('progress-bar-wrapper');
        wrapper.innerHTML='';
        this.createHTML(wrapper, stages, currentStage);
      }
    }
// ------------------------- for timeline -----------------


// ------------------------ Form Display --------------
displayForm()
{
  this.selectedPermit=JSON.parse(this.sstorage.retrieve("selectedPermit"));
  this.form=JSON.parse(this.selectedPermit.formJSON);
}


getPenalty()
{
  console.log("what");
  this.penalties=[];
  this.userservice.getMyPenaltyPermits(JSON.parse(this.sstorage.retrieve("selectedPermit")).permitNumber).subscribe(data=>{
    this.penalties.push(data);
    console.log(data);
  });
}


getSuspension(){
  this.suspensions=[];
  this.userservice.getMySuspendedPermit(JSON.parse(this.sstorage.retrieve("selectedPermit")).permitNumber).subscribe(data=>{
    this.suspensions=data;
    console.log(data);
  });
}
getRevocation(){
  this.revocations=[];
  this.userservice.getMyRevokedPermit(JSON.parse(this.sstorage.retrieve("selectedPermit")).permitNumber).subscribe(data=>{
    this.revocations=data;
    console.log(data);
  });
}

}
