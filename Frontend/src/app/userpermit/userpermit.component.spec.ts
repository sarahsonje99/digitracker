import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserpermitComponent } from './userpermit.component';

describe('UserpermitComponent', () => {
  let component: UserpermitComponent;
  let fixture: ComponentFixture<UserpermitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserpermitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserpermitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
