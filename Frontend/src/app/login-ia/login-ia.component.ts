import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { UserService } from '../shared/service/user.service';

@Component({
  selector: 'app-login-ia',
  templateUrl: './login-ia.component.html',
  styleUrls: ['./login-ia.component.css']
})
export class LoginIaComponent implements OnInit {

  username= new FormControl('');
  ianame = new FormControl('');
  password = new FormControl('');

  constructor(private http: Http, private router: Router,private sstorage: SessionStorageService,private userservice:UserService) { }

  ngOnInit() {
  }

  invalidDetails() {
    // let length = this.password.value.length;
    // if(length < 4)
    //   return false;
    // else if(this.password.value.search(/\d/) == -1)
    //   return false;
    // else if (this.password.value.search(/[a-z]/) == -1) 
    //   return false;
    // else if (this.password.value.search(/[A-Z]/) == -1)
    //   return false;
    // else if (this.password.value.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1)
    //     return false;
    // else
      return true;
  }

  loginIA() {
    // Rest API call for login of Issuing Authority
    this.http.post('http://localhost:4100/api/loginia', {
      username: this.username.value,
      ianame: this.ianame.value,
      password: this.password.value
    }).subscribe((data:any) => {
      console.log(data)
      let data1 = JSON.parse(data._body)
      if(data1.auth == true) {
        this.sstorage.store('ia',this.ianame.value);
        this.sstorage.store("loggedInIA",true);
        this.userservice.loggedIA.emit(true);
        console.log("LOGIN SUCCESSFUL");
        this.router.navigate(['formgen']);
      }
    });
    
  }

}
