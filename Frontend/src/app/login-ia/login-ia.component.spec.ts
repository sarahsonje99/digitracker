import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginIaComponent } from './login-ia.component';

describe('LoginIaComponent', () => {
  let component: LoginIaComponent;
  let fixture: ComponentFixture<LoginIaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginIaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginIaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
