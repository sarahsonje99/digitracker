import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsRtoComponent } from './forms-rto.component';

describe('FormsRtoComponent', () => {
  let component: FormsRtoComponent;
  let fixture: ComponentFixture<FormsRtoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormsRtoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsRtoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
