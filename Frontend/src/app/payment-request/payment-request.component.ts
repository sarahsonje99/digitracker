import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Http } from '@angular/http';

declare let paypal: any;

@Component({
  selector: 'app-payment-request',
  templateUrl: './payment-request.component.html',
  styleUrls: ['./payment-request.component.css']
})
export class PaymentRequestComponent implements OnInit {

  pricing = new FormControl('');
  data: any;
  url:any;

  constructor(private http: Http) { }

  ngOnInit() {
    
  }

  pay() {
    this.http.get("http://localhost:4100/api/pay/"+this.pricing.value)
    .subscribe((data: any) =>{
      this.data = JSON.parse(data._body);
      this.url = this.data.url;
      console.log(this.url)
      window.open(this.url,"_self");
    },error=>{
        console.log(error);
    });
  }
}
