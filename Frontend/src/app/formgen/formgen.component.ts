import { Component, OnInit } from '@angular/core';
import { config, defaultI18n, defaultOptions } from "../formbuilder/config";
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { FormBuilderCreateor } from "../formbuilder/form-builder";
import I18N from "../formbuilder/mi18n";
import {RtoForm} from '../shared/model/form.model';
import * as JQuery from 'jquery';
// import 'jqueryui/jquery-ui.min.js';
import * as ffRender from "../formbuilder/formrender.js";
import { UserService } from '../shared/service/user.service';
import { FormService } from '../shared/service/form.service';
import { SessionStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

declare var jquery:any;
declare var $ :any;

function initR(){
(function($) {
  (<any>$).fn.formRender = function(options) {
    if (!options) {
      options = {};
    }
    let elems = this;
    let formRender = new ffRender.FormRender(options).render();
    return formRender;
  };
})(jQuery);
}
function initJq() {
  (function ($) {
  
    (<any>$.fn).formBuilder = function (options) {
      if (!options) {
        options = {};
      }
      let elems = this;
      let {i18n, ...opts} = $.extend({}, defaultOptions, options, true);
      (<any>config).opts = opts;
      let i18nOpts = $.extend({}, defaultI18n, i18n, true);
      let instance = {
        actions: {
          getData: null,
          setData: null,
          save: null,
          showData: null,
          setLang: null,
          addField: null,
          removeField: null,
          clearFields: null
        },
        get formData() {
          return instance.actions.getData('json');
        },
        get save(){
          return instance.actions.save();
        },

        promise: new Promise(function (resolve, reject) {
          new I18N().init(i18nOpts).then(() => {
            elems.each(i => {
              let formBuilder = new FormBuilderCreateor().getFormBuilder(opts, elems[i]);
              $(elems[i]).data('formBuilder', formBuilder);
              instance.actions = formBuilder.actions;
            });
            delete instance.promise;
            resolve(instance);
          }).catch(console.error);
        })

      };

      return instance;
    };
    
  })(jQuery);
}

@Component({
  selector: 'app-formgen',
  templateUrl: './formgen.component.html',
  styleUrls: ['./formgen.component.css']
})

export class FormgenComponent implements OnInit {

  docList:string[]=[];  //doclist
  validreqlist:string[]=[];   //validation requirement list
  //STEPPER
  permit: FormGroup;
  metadata: FormGroup;
  thirdFormGroup: FormGroup;
  isOptional = false;

  permitMetaData: any;    ///the permitMetaData that will be posted = initialized in ngoninit
  previousPermits: any;

  formGen: boolean;

  permitname = new FormControl('');

  bool: boolean;
  controls: any;
  vr=false;

  formBuilder: any;
  formrender: ffRender.FormRender;
  formuserrender: ffRender.FormRender;

  permitNames=[];
  chosenPermits=[];
  ngOnInit(): void {
    this.permitMetaData = {
      "$class":"digitracker.network.PermitMetadata",
      "permitName": "",
      "requiredDocs": [],
      "formBuilderJSON": "",
      "validationRequired": false,
      "validationRequirements": [],
      "requiredPermitNames": [],
      "validityPeriod": 0,
      "applicationFee": 0,
      "IAName": ""
    }

    this.bool=true;
    this.formGen = false;
    initJq(); 
    
    this.userservice.allPermitsMetaData().subscribe(data=>{
      this.permitNames.push(data['permitName']);
      this.chosenPermits.push(0);
    });

    // this.formservice.getDependency().subscribe((data:any) => {
    //   console.log(JSON.parse(data._body))
    //   this.previousPermits = JSON.parse(data._body)
    // });
  }

  constructor(private router:Router,private userservice:UserService, private formservice: FormService, private _formBuilder: FormBuilder,private sstorage:SessionStorageService){

    // initialization of stepper
    this.permit = this._formBuilder.group({
      permitname: ['', Validators.required]
    });
    this.metadata = this._formBuilder.group({
      verificationRequired: false,
      validationRequired: false,
      validationRequirements: [],
      requiredPermitNames: new FormArray([]),
      validityPeriod: '',
      applicationFee: ''
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ''
    });

  }

  //  called when entering permitname in step1
  addPermit() {
    this.permitMetaData.IAName=this.sstorage.retrieve('ia');
    this.permitMetaData.permitName = this.permit.value.permitname;
    // console.log(this.permitMetaData.permitName);
    // console.log("PREVIOUS: "+this.previousPermits[0].permitName);
  }
  // if no permitname is entered
  invalidDetails() {
    if(this.permitname.value.length==0 )
      return true;
    else
      return false;
  }

  // event for radio button activation
  radioChange()
  {
    if(this.metadata.value.validationRequired==true)
    {
        this.vr=true;
    }
    else{
        this.vr=false;
    }
  }

  // dynamic addition of validation required list
  addValidationReq()
  {
    //Create an input type dynamically.
	var element = document.createElement("input");

	//Assign different attributes to the element.
	element.setAttribute("type", "text");
	element.setAttribute("class", "validreq");


	var validationReq = document.getElementById("validationReq");

	//Append the element in page (in span).
  validationReq.appendChild(element);
  validationReq.appendChild(document.createElement("br"));
  validationReq.appendChild(document.createElement("br"));
  }

  // selection of dependent permit names
  sendPermitName(pname)
  {
    console.log(pname);
    this.chosenPermits[this.permitNames.indexOf(pname)]=(this.chosenPermits[this.permitNames.indexOf(pname)]+1)%2;
  }

    // dynamic addition of document addition
  addDocs()
  {
    var element = document.createElement("input");

	//Assign different attributes to the element.
	element.setAttribute("type", "text");
	element.setAttribute("class", "doclist");


	var docs = document.getElementById("addDocs");

	//Append the element in page (in span).
  docs.appendChild(element);
  docs.appendChild(document.createElement("br"));
  docs.appendChild(document.createElement("br"));
  }

  // on submission of metadata in step2 
  generateForm() {

    // for retrieving validation requirements
    var validreq=document.getElementsByClassName("validreq");
    this.validreqlist=[];
    for(var i = 0; i < validreq.length; i++)
    {
        this.validreqlist.push((<HTMLInputElement>validreq[i]).value);
    }   

    // for retrieving document list
    var docs=document.getElementsByClassName("doclist");
    this.docList=[];
    for(var i = 0; i < docs.length; i++)
    {
        this.docList.push((<HTMLInputElement>docs[i]).value);
    }   

    // 
    // assigning metadata received to permitMetaDats
    this.permitMetaData.requiredDocs=this.docList;
 
    if(this.validreqlist!=[])
    {
      this.permitMetaData.validationRequired=true;
      this.permitMetaData.validationRequirements=this.validreqlist;
    }

    this.permitMetaData.applicationFee=this.metadata.value.applicationFee;

    this.permitMetaData.validityPeriod=this.metadata.value.validityPeriod;

    for(var i=0;i<this.permitNames.length;i++)
    {
      if(this.chosenPermits[i]==1)
      {
        this.permitMetaData.requiredPermitNames.push(this.permitNames[i]);
      }
    }
    console.log(this.permitMetaData.requiredPermitNames);
    // 
    // 
    
    // generates formbuilder
    if(this.formGen == false){
      this.formGen = true
      this.bool = false;
      this.formBuilder = (<any>jQuery('.build-wrap')).formBuilder();
      console.log(this.formBuilder);
    }
  }

  submitForm() {
    var x =this.formBuilder.actions.getData('json');
    console.log(this.formBuilder.actions.getData('json'));
    console.log(JSON.stringify(x));
    this.permitMetaData.formBuilderJSON=JSON.stringify(x);
    this.postForm();
  
  }

  postForm() {
    console.log(JSON.stringify(this.permitMetaData));
    this.userservice.postFormFormat(JSON.stringify(this.permitMetaData)).subscribe(data=>{
       this.userservice.reloadnav.emit(true);
       this.router.navigate(['']);
    });
  }
 
  

}





