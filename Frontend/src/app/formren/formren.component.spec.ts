import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormrenComponent } from './formren.component';

describe('FormrenComponent', () => {
  let component: FormrenComponent;
  let fixture: ComponentFixture<FormrenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormrenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
