import { Component, OnInit, OnDestroy } from '@angular/core';
import { config, defaultI18n, defaultOptions } from "../formbuilder/config";
import { FormBuilderCreateor } from "../formbuilder/form-builder";
import I18N from "../formbuilder/mi18n";
import * as JQuery from 'jquery';
import 'jqueryui/jquery-ui.min.js';
import * as ffRender from "../formbuilder/formrender.js";
import { UserService } from '../shared/service/user.service';
import { SessionStorageService } from 'ngx-webstorage';
import { FormControl } from '@angular/forms';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';



declare var jquery:any;
declare var $ :any;
function initR(){
  (function($) {
    (<any>$).fn.formRender = function(options) {
      if (!options) {
        options = {};
      }
      let elems = this;
      let formRender = new ffRender.FormRender(options).render();
      return formRender;
    };
  })(jQuery);
  }

@Component({
  selector: 'app-formren',
  templateUrl: './formren.component.html',
  styleUrls: ['./formren.component.css']
})
export class FormrenComponent implements OnInit,OnDestroy {

  requestForMeta={"$class":"digitracker.network.GetApplicationForm","permitName":"","aadhaarId":"","timestamp":""};

  files:any;
  filestring:any;

 // docs={"sign":false,"addressProof":false};
 docs={}; 
 submissionDocsFinal={};
  submissionDocsS={"sign":""};
  submissionDocsSA={"sign":"","addressProof":""};
  submissionDocsA={"addressProof":""};
  submissionDocsN={};


  paidSuccessful: boolean;
  url:any;
  selectedCentre = new FormControl('');
  pricing:number=0;
  paymentId: any;
  PayerID: any;

  postSelectedData:{"$class":string,"aadhaarId":string,"permitName":string,"formJSON":string,"IACentreName":string,"submittedDocsJSON":string,"timestamp":string}=
  {"$class":"digitracker.network.SubmitApplication","aadhaarId":"",permitName:"","formJSON":"","IACentreName":"","submittedDocsJSON":"","timestamp":""};
  
  //submission:{permitName:string,formJSON:string,aadhaarId:string,IACentre:string}={permitName:"",formJSON:"",aadhaarId:"",IACentre:""};
  formrender1: ffRender.FormRender;
  data=[];
  permitMetadata='';
  centres;
  constructor(private router:Router,private sstorage:SessionStorageService,private userservice:UserService, private http: Http, private route: ActivatedRoute) {

    // After Payment is successful, the user is redirectd to this page. Check the paymentID and PayerID to see if payment is successful or not 
    this.route.queryParams.subscribe(params => {
      this.paymentId = params['paymentId'];
      this.PayerID = params['PayerID'];
      if(this.paymentId != undefined && this.PayerID != undefined){
        console.log(this.paymentId);
        console.log(this.PayerID);
        this.http.post("http://localhost:4100/api/pay-success", {
          "paymentId": this.paymentId,
          "PayerID": this.PayerID
        })
        .subscribe((data:any) => {
          console.log(JSON.parse(data._body).state);

          if(JSON.parse(data._body).state == "approved") {
            console.log(this.sstorage.retrieve("submission"));
            console.log(this.sstorage.retrieve('loggedIn'));
            // this.paidSuccessful = true;
            // POST REQUEST HERE

            this.userservice.postData(this.sstorage.retrieve("submission")).subscribe(res=>{
              console.log(res);
              this.userservice.reloadnav.emit(true);
              this.router.navigate(['']);
            });
            // CLear the session storage
            // this.sstorage.clear("submission");
          }
        });
      }
    });
    
    
    this.userservice.openPermitMetaData.subscribe(data=>{
      if(data==true) {
        console.log("true");
        this.fetchForm();
      }
    });

    // this.userservice.getCentres().subscribe(data=>{
    //     this.centres.push(data);
    // });
  }

  ngOnInit() {
    this.fetchForm();
    this.paidSuccessful = false;
  }
  // ------------------------for form fetching-----------------
  fetchForm()
  {
    this.requestForMeta.timestamp=new Date().toISOString();
    this.requestForMeta.aadhaarId=this.sstorage.retrieve('userId');
    this.requestForMeta.permitName=this.sstorage.retrieve('permitMetadataName');
    // this.userservice.getCentres(this.sstorage.retrieve('IAName')).subscribe(data=>{
    //     this.centres.push(data);
    // });
    this.userservice.getSpecificPermitMetadata(JSON.stringify(this.requestForMeta)).subscribe(data=>{
      this.centres=JSON.parse(data).IACentreNames;
      
      this.docs=JSON.parse(JSON.parse(data).docsFound);
      console.log(this.docs);
      // console.log(JSON.parse(data).IACentreNames);
      this.permitMetadata=data;
      this.renderForm();
     
      console.log(this.permitMetadata);
    });
  }

  // ------------------------ for form rendering --------------

  renderForm() {
   var keys=["sign","addressProof"];
   console.log(this.docs);
    for(var k in keys)
    {
      var dockey=Object.keys(this.docs);
      var exist=false;
      for(var dk in dockey)
      {
          if(keys[k]==dockey[dk])
          {
              exist=true;
              if( this.docs[keys[k]] == true)
              {
                var el=document.getElementById(keys[k]);
                el.parentNode.removeChild(el);
              }
              break;
          }
      }
      if(exist==false)
      {
        var el=document.getElementById(keys[k]);
        el.parentNode.removeChild(el); 
      }   
    }

    for(var k in Object.keys(this.docs))
    {

    }
    if(this.docs["sign"]==true || !this.docs['sign'])
    {
      if(this.docs["addressProof"]==true || !this.docs['addressProof'])
      {
        this.submissionDocsFinal=this.submissionDocsN;
      }
      else{
        this.submissionDocsFinal=this.submissionDocsA;
      }
    }else{
      if(this.docs["addressProof"]==true || !this.docs['addressProof'])
      {
        this.submissionDocsFinal=this.submissionDocsS;
      }
      else{
        this.submissionDocsFinal=this.submissionDocsSA;
      }
    }
    console.log(JSON.parse(this.permitMetadata).permitMetadata);
    //this.permitMetadata=JSON.parse(this.sstorage.retrieve("permitMetadata"));
    this.pricing=JSON.parse(JSON.parse(this.permitMetadata).permitMetadata).applicationFee;
    if(this.pricing === null)
      this.pricing = 3000;
    const formRenderOpts = {
      container:(<any>jQuery('.wrap')),
      formData:JSON.parse(JSON.parse(JSON.parse(this.permitMetadata).permitMetadata).formBuilderJSON),
      dataType: 'json',
      render:true
    };

    this.formrender1= new ffRender.FormRender(formRenderOpts).render();  
  }


  getFiles(event,id) {
   this.sstorage.store('fileid',id);
    this.files = event.target.files;
    
    var reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsBinaryString(this.files[0]);
    //alert(this.files);
   
    //alert(this.files[0]);
}

_handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.filestring = btoa(binaryString);  // Converting binary string data.
   //alert(this.filestring);
   var id=this.sstorage.retrieve('fileid');
   this.submissionDocsFinal[id]=this.filestring;
   console.log(this.submissionDocsFinal);
   this.sstorage.clear('fileid');
   this.filestring='';
}



  postData() {
    ////---------checking all files
    var allfiles=true;
    var keys=Object.keys(this.submissionDocsFinal);
    console.log(keys);
    for(var k in keys)
    {
      if(this.submissionDocsFinal[keys[k]] == '')
      {
         allfiles=false;
         break; 
      }
    }
    ////---------checking all form data
    var x;
    this.data=[];
    console.log(this.formrender1.userData);
    for(x in this.formrender1.userData) {
      if(this.formrender1.userData[x].required==true) {
        if( (this.formrender1.userData[x].userData==undefined || this.formrender1.userData[x].userData[0]=="")) {
          alert('missed value');
          this.data=[];
          break;
        }
      }
      else if(this.formrender1.userData[x].userData==undefined) {
        this.formrender1.userData[x].userData="";
      }
      this.data.push({label:this.formrender1.userData[x].label,value:this.formrender1.userData[x].userData});
    }
    
    if(this.data !=[] && this.selectedCentre.value!="" && allfiles==true) {
   
          this.postSelectedData.permitName=this.sstorage.retrieve('permitMetadataName');
          this.postSelectedData.aadhaarId=this.sstorage.retrieve("userId");
          this.postSelectedData.timestamp=new Date().toISOString();
          this.postSelectedData.IACentreName=this.selectedCentre.value;
          this.postSelectedData.formJSON=JSON.stringify(this.data);
          this.postSelectedData.submittedDocsJSON=JSON.stringify(this.submissionDocsFinal);
          console.log(this.postSelectedData);
          this.sstorage.store("submission",JSON.stringify(this.postSelectedData));
          this.pay();
          // console.log(this.sstorage.retrieve("submission"));

    }    
  }

  pay() {
    this.http.get("http://localhost:4100/api/pay/"+this.pricing)
    .subscribe((data: any) =>{
      this.data = JSON.parse(data._body);
      this.url = this.data['url'];
      console.log(this.url)
      window.open(this.url,"_self");
    },error=>{
        console.log(error);
    });
  }

  ngOnDestroy() {
    this.sstorage.clear('permitMetadataName');
  }

}
