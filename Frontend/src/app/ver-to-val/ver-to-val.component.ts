import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../shared/service/user.service';
import { SessionStorageService } from 'ngx-webstorage';
import { FormControl } from '@angular/forms';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-ver-to-val',
  templateUrl: './ver-to-val.component.html',
  styleUrls: ['./ver-to-val.component.css']
})
export class VerToValComponent implements OnInit {

  chosenPermit=[];
  validationReq={};
  validationDates={};
  keys=[];
  postData:{"$class":string,"permitName":string,"aadhaarId":string,"validationRequirementsJSON":string,"validationDatesJSON":string,"timestamp":string}={"$class":"digitracker.network.ValidateNextStage","permitName":"","aadhaarId":"","validationRequirementsJSON":"","validationDatesJSON":"","timestamp":""};

  rejectData:{"$class":string,"rejectionStage":string,"rejectedBecause":string,"permitName":string,"aadhaarId":string,"timestamp":string}={"$class":"digitracker.network.RejectApplication","rejectionStage":"VALIDATED","rejectedBecause":"","permitName":"","aadhaarId":"","timestamp":""}; 
   showRejectionDesc:boolean=false;
  Description = new FormControl('');
  
  constructor(private router:Router,private userservice:UserService,private sstorage:SessionStorageService) 
  {
    this.userservice.openVerifiedForm.subscribe(data=>{
      if(data==true){
         this.displayvalidations();
      }
    }); 
  }

  ngOnInit() {
    this.displayvalidations();
  }

  displayvalidations()
  {
    var x=this.sstorage.retrieve('chosenVerifiedPermit');
    var y=x.split("#");
    this.userservice.reviewSpecificPermit(y[0],y[1]).subscribe(data=>{
      console.log(data);
      this.chosenPermit.push(data);
      this.validationReq=JSON.parse(data.validationRequirementsJSON);
      this.keys=Object.keys(JSON.parse(data.validationRequirementsJSON));
    });
  }

  sendValidationStage(k)
  {
    var x=0;
    console.log(k);
    for(var i in this.validationReq)
    {
      if(i==k)
      {
        this.validationDates[i]=document.getElementsByClassName('dates')[x]['value'];
        console.log(this.validationDates);
      }
        x++;
    }
    this.validationReq[k]=true;
    this.postData.permitName=this.chosenPermit[0].permitName;
    this.postData.aadhaarId=this.chosenPermit[0].aadhaarId;
    this.postData.validationDatesJSON=JSON.stringify(this.validationDates);
    this.postData.validationRequirementsJSON=JSON.stringify(this.validationReq);
    this.postData.timestamp=new Date().toISOString();
    this.userservice.postValidatedStage(JSON.stringify(this.postData)).subscribe(data=>{
      console.log(data);
      this.sstorage.clear('chosenVerifiedPermit');
      this.router.navigate(['forms-rto']);
    });
  }

  rejectDescription()
  {
    this.showRejectionDesc=true;
  }
  
  finalReject()
  {
    if(this.Description.value!='')
    {
      this.rejectData.rejectedBecause=this.Description.value;
      this.rejectData.permitName=this.chosenPermit[0].permitName;
      this.rejectData.aadhaarId=this.chosenPermit[0].aadhaarId;
      this.rejectData.timestamp=new Date().toISOString();
      this.userservice.rejectApplication(JSON.stringify(this.rejectData)).subscribe(data=>{
          console.log(data);
          this.sstorage.clear('chosenVerifiedPermit');
          this.router.navigate(['forms-rto']);
      });
    }
    
  }

}
