import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerToValComponent } from './ver-to-val.component';

describe('VerToValComponent', () => {
  let component: VerToValComponent;
  let fixture: ComponentFixture<VerToValComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerToValComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerToValComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
