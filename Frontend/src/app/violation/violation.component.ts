import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { UserService } from '../shared/service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-violation',
  templateUrl: './violation.component.html',
  styleUrls: ['./violation.component.css'],
  providers:[DatePipe]
})
export class ViolationComponent implements OnInit {

  suspendBoolean: boolean;
  revokedBoolean: boolean;
  noneBoolean: boolean;

  description = new FormControl('');
  fee = new FormControl('');
  actionTaken = new FormControl('');
  permitNumber = new FormControl('');
  date = new FormControl('');
  suspensionPeriod = new FormControl('');
  actions = ['Suspended', 'Revoked', 'None'];
  postViolationNone;
  postViolationSuspend;
  postViolationRevoke;
  constructor(private router:Router,private datePipe: DatePipe, private userService: UserService) { }

  ngOnInit() {
    this.postViolationNone={"$class":"digitracker.network.RegisterPenalty","permitNumber":"","description":"","penalty":"","date":"","timestamp":""};
    this.postViolationSuspend={"$class":"digitracker.network.SuspendPermit","permitNumber":"","suspendedBecause":"","suspendTill":"","date":"","timestamp":""};
    this.postViolationRevoke={"$class":"digitracker.network.RevokePermit","permitNumber":"","revokedBecause":"","date":"","timestamp":""};
  }

  submit(){
    var violationForm;

    if(this.actionTaken.value === "Suspended") {

      this.postViolationSuspend.permitNumber=this.permitNumber.value;
      this.postViolationSuspend.suspendedBecause=this.description.value;
      this.postViolationSuspend.suspendTill=this.suspensionPeriod.value;
      this.postViolationSuspend.date=this.transformDate(this.date.value);
      this.postViolationSuspend.timestamp=new Date().toISOString();
      console.log(JSON.stringify(this.postViolationSuspend));
      this.userService.postViolationSuspend(JSON.stringify(this.postViolationSuspend)).subscribe((data: any) => {
        console.log(JSON.stringify(data));
        this.router.navigate(['forms-rto']);
      });
    }
    else if(this.actionTaken.value === "Revoked"){

      this.postViolationRevoke.permitNumber=this.permitNumber.value;
      this.postViolationRevoke.revokedBecause=this.description.value;
      this.postViolationRevoke.date=this.transformDate(this.date.value);
      this.postViolationRevoke.timestamp=new Date().toISOString();
      console.log(this.postViolationRevoke);
      this.userService.postViolationRevoke(JSON.stringify(this.postViolationRevoke)).subscribe((data: any) => {
        console.log(JSON.stringify(data));
        this.router.navigate(['forms-rto']);
      });
    }
    else if(this.actionTaken.value === "None"){

      this.postViolationNone.permitNumber=this.permitNumber.value;
      this.postViolationNone.description=this.description.value;
      this.postViolationNone.penalty=this.fee.value;
      this.postViolationNone.date=this.transformDate(this.date.value);
      this.postViolationNone.timestamp=new Date().toISOString();

      this.userService.postViolationNone(JSON.stringify(this.postViolationNone)).subscribe((data: any) => {
        console.log(JSON.stringify(data));
       this.router.navigate(['forms-rto']);
      });
    }
  }

  transformDate(date) {
    return this.datePipe.transform(date, 'dd-MM-yyyy'); //whatever format you need.
  }

  setRadio(action) {
    if(action==="Suspended") {
      this.suspendBoolean = true;
      this.revokedBoolean = false;
      this.noneBoolean = false;
    }
    else if(action==="Revoked") {
      this.revokedBoolean = true;
      this.suspendBoolean = false;
      this.noneBoolean = false;
    }
    else if(action==="None"){
      this.noneBoolean = true;
      this.revokedBoolean = false;
      this.suspendBoolean = false;
    }
    else {
      this.noneBoolean = false;
      this.revokedBoolean = false;
      this.suspendBoolean = false;
    }
  }

}
