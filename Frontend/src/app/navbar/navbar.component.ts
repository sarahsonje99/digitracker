import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from '../shared/service/user.service';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { userInfo } from 'os';
import { LoginUserComponent } from '../login-user/login-user.component';
import { RegisterComponent } from '../register/register.component';

import { WebsocketService } from './../websocket.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers:[SessionStorageService]
})
export class NavbarComponent implements OnInit{
    permitNames:{"key":string,"value":string[]}[]=[];

    private iter: number = 0;
    private messageFromServer: any = [];
    private notifications: any = [];
    private header: any=[];
    private message: any=[];
    private transactionId: any=[];

    showall:boolean=false;
    loggedUser:boolean=false;
    loggedIA:boolean=false;
    loggedIACentre:boolean=false;
    mypermitlist=[];
    permitsall=[];
  constructor(public dialog: MatDialog,private sstorage:SessionStorageService,private userservice:UserService,private router:Router,private breakpointObserver: BreakpointObserver, private wsService: WebsocketService) {
    this.userservice.reloadnav.subscribe(data=>{
      if(data==true)
      {
        this.ngOnInit();
      }
    });
    this.userservice.logging.subscribe(data=>{
      if(data==true)
      {
        this.loggedUser=true;
      }
      if(data==false)
      {
        this.loggedUser=false;
      }
    });
    this.userservice.loggedIA.subscribe(data=>{
      if(data==true)
      {
        this.loggedIA=true;
      }
      if(data==false)
      {
        this.loggedIA=false;
      }
    });
    this.userservice.loggedIACentre.subscribe(data=>{
      if(data==true)
      {
        this.loggedIACentre=true;
      }
      if(data==false)
      {
        this.loggedIACentre=false;
      }
    });
    
    this.wsService.createObservableSocket("ws://192.168.225.81:3000")
    .subscribe(data => {
      if(this.sstorage.retrieve("userid") === JSON.parse(data).aadhaarId) {
        this.notifications.push({
          "header":JSON.parse(data).$class.split('.')[2].toString(),
          "message":JSON.parse(data).permitName,
          "timesatamp":JSON.parse(data).timesatamp
        })
        console.log(this.notifications);
      }
    });

  }
  ngOnInit()
  {
    if(this.sstorage.retrieve('loggedIn')==true)
    {
      this.loggedUser=true;
      this.loggedIA=false;
      this.loggedIACentre=false;
    }else if(this.sstorage.retrieve('loggedInIA')==true)
    {
      this.loggedUser=false;
      this.loggedIA=true;
      this.loggedIACentre=false;
    }else if(this.sstorage.retrieve('loggedInIACentre')==true)
    {
      this.loggedUser=false;
      this.loggedIA=false;
      this.loggedIACentre=true;
    }
    if(this.loggedUser==true)
    {
      console.log("hi");
      this.mypermitlist=[];
      this.userservice.listOfUserPermits(this.sstorage.retrieve("userId")).subscribe(data=>{
        this.mypermitlist.push(data);
      });
      this.permitsall=[];
      this.userservice.allPermitsMetaData().subscribe(data=>{
        this.permitsall.push(data);
       console.log(this.permitsall);
       this.getPermitNames();
      });
    }
   
  }

  openLoginDialog(): void {
    const dialogRef = this.dialog.open(LoginUserComponent, {
      width: '400px',
      height:'300px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openRegisterDialog(): void {
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '400px',
      height:'300px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getPermitNames()
  {
    this.permitNames=[];
    console.log(this.permitsall);
    var ianames=[];
    var unique = {};
    var distinct = [];
        for( var i in this.permitsall ){
         if( typeof(unique[this.permitsall[i].IAName]) == "undefined"){
          distinct.push(this.permitsall[i].IAName);
         }
         unique[this.permitsall[i].IAName] = 0;
        }
    for(var i in distinct)
    {
      var arr=[];
      for(var j in this.permitsall)
      {
        
        if(distinct[i]==this.permitsall[j].IAName)
        {
            arr.push(this.permitsall[j].permitName);
        }
      }
      this.permitNames.push({"key":distinct[i],"value":arr});
    }
    this.showall=true;
    console.log(this.permitNames);
  }

  markAsRead(notification){
    this.notifications.pop(notification);
  }

  goToMyPermit(selectedPermit){
    console.log(selectedPermit);
    this.sstorage.clear('selectedPermit');
    this.sstorage.store('selectedPermit',JSON.stringify(selectedPermit));
    this.userservice.openUserPermit.emit(true);
      this.router.navigate(['mypermit']);
      //this.router.navigate(['mypermit']);
  }

  goToAllPermit(permit,chosenIA)
  {
    console.log(this.sstorage.retrieve("userId"));
    var x;
    var y;    //for marking which form it is if it exist in userpermits
    var permitexist=false;
    var selectedPermit;
    console.log(permit);
    for (x in this.mypermitlist)
    {
      console.log(this.mypermitlist[x].permitName);
      if(permit==this.mypermitlist[x].permitName)
      {
        selectedPermit=this.mypermitlist[x];
        //console.log(permit.permitName);
        y=x;
        permitexist=true;
        break;
      }
    }
    if(permitexist==false)
    {
     
      this.sstorage.store("permitMetadataName",permit);
      this.sstorage.store("chosenIA",chosenIA);
      // console.log(JSON.stringify(permit));
      
      this.router.navigate(['formren']);
      this.userservice.openPermitMetaData.emit(true);
    }else{
      console.log(selectedPermit);
      this.goToMyPermit(selectedPermit);
      // console.log(selectedPermit);
      // this.sstorage.clear('selectedPermit');
      // this.sstorage.store('selectedPermit',JSON.stringify(selectedPermit));
      // this.userservice.openUserPermit.emit(true);
      // this.router.navigate(['mypermit']);
    }
  }

  createPermit() {
    this.router.navigate(['formgen']);
    this.userservice.openPermitMetaData.emit(true);
  }

  viewApplications() {
    this.router.navigate(['forms-rto']);
  }

  addViolations() {
    this.router.navigate(['violation']);
  }

  logOut()
  {
    console.log("log");
    this.loggedUser=this.loggedIA=this.loggedIACentre=false;
    this.userservice.logging.emit(false);
    this.userservice.loggedIA.emit(false);
    this.userservice.loggedIACentre.emit(false);
    this.sstorage.clear();
    console.log(this.sstorage.retrieve('permitMetadataName'));
    this.router.navigate(['']); 
  }


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches)
  );

}
