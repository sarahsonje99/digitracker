/* ====================NETWORK STUFF====================*/
rule NetworkAdminUser {
    description: "Grant business network administrators full access to user resources"
    participant: "org.hyperledger.composer.system.NetworkAdmin"
    operation: ALL
    resource: "**"
    action: ALLOW
}

rule EverybodyCanReadNetwork {
    description: "Grant business network administrators full access to system resources"
    participant: "ANY"
    operation: READ
    resource: "org.hyperledger.composer.system.**"
    action: ALLOW
}

rule NetworkAdminSystem {
    description: "Grant business network administrators full access to system resources"
    participant: "org.hyperledger.composer.system.NetworkAdmin"
    operation: ALL
    resource: "org.hyperledger.composer.system.**"
    action: ALLOW
}

rule ReadNetwork{
  description: "allow participants to connect to network"
  participant: "ANY"
  operation: ALL
  resource: "org.hyperledger.composer.system.Network"
  action: ALLOW 
}

rule historianAccess{
  description: "Only allow members to read historian records referencing transactions they submitted."
  participant(p): "ANY"
  operation: READ
  resource(r): "org.hyperledger.composer.system.HistorianRecord"
  condition: (r.participantInvoking.getIdentifier() == p.getIdentifier())
  action: ALLOW
}

rule historianCreateAccess{
  description: "Only allow members to create historian records in order to submit transactions"
  participant: "ANY"
  operation: CREATE
  resource: "org.hyperledger.composer.system.HistorianRecord"
  action: ALLOW
}

/*==========================FOR ALL PARTICIPANTS================*/
rule AllReadPermitMetadata{
  description: "Grant Issuing Authority right to access form metadata"
  participant: "ANY"
  operation: READ
  resource: "digitracker.network.PermitMetadata"
  //condition:(i.IAname == r.IAname)
  action: ALLOW
}

rule AllCanAccessThemselves{
  description: "Every participant can CRUD on themselves"
  participant(x): "org.hyperledger.composer.system.Participant"
  operation: ALL
  resource(y): "org.hyperledger.composer.system.Participant"
  condition:(x.getIdentifier() == y.getIdentifier())
  action: ALLOW
}

rule AllCanAccessPermitMetadata{
  description: "All can access all permitMetadata"
  participant: "ANY"
  operation: ALL
  resource: "digitracker.network.PermitMetadata"
  action: ALLOW
}

/*==========================FOR CITIZEN======================*/
rule CitizenReadsUserPermit{
  description: "Grant citizen right to view his own permit"
  participant(c): "digitracker.network.Citizen"
  operation: READ
  resource(r): "digitracker.network.UserPermit"
  condition:(c.aadhaarId==r.aadhaarId)
  action: ALLOW
}

rule CitizenSubmitApplication{
   description:"Citizen controls submission transaction"
   participant:"digitracker.network.Citizen"
   operation: ALL
   resource:"digitracker.network.SubmitApplication"
   action: ALLOW 
}

rule CitizenGetApplicationForm{
   description:"Citizen controls submission transaction"
   participant:"digitracker.network.Citizen"
   operation: ALL
   resource:"digitracker.network.GetApplicationForm"
   action: ALLOW 
}

rule CitizenCreateUserPermit{
   description:"Citizen can create user permits in SubmitApplication transaction"
   participant:"digitracker.network.Citizen"
   operation: CREATE
   resource:"digitracker.network.UserPermit"
   transaction: "digitracker.network.SubmitApplication"
   action: ALLOW 
}

/*==========================FOR ISSUING_AUTHORITY===================*/
rule IssueAuthorityAccessPermitMetadata{
  description: "Grant Issuing Authority right to access form metadata"
  participant(i): "digitracker.network.IssuingAuthority"
  operation: ALL
  resource(r): "digitracker.network.PermitMetadata"
  condition:(i.IAname == r.IAname)
  action: ALLOW
}

rule IssuingAuthorityReadCitizensVerify {
  description: "Issuing authorities can READ citizens when appropriate"
  participant: "digitracker.network.IssuingAuthority"
  operation: READ
  resource: "digitracker.network.Citizen"
  transaction: "digitracker.network.VerifyApplication"
  action: ALLOW
}

rule IssuingAuthorityReadCitizensValidate {
  description: "Issuing authorities can READ citizens when appropriate"
  participant: "digitracker.network.IssuingAuthority"
  operation: READ
  resource: "digitracker.network.Citizen"
  transaction: "digitracker.network.ValidateNextStage"
  action: ALLOW
}

rule IssuingAuthorityReadCitizensIssue {
  description: "Issuing authorities can READ citizens when appropriate"
  participant: "digitracker.network.IssuingAuthority"
  operation: READ
  resource: "digitracker.network.Citizen"
  transaction: "digitracker.network.IssuePermit"
  action: ALLOW
}

/*==========================FOR ISSUING_AUTHORITY_CENTRE================*/
rule ReadUserPermitIAC{
  description: "Grant IAC right to manipulate userpermit "
  participant(i): "digitracker.network.IssuingAuthorityCentre"
  operation: ALL
  resource(r): "digitracker.network.UserPermit"
  condition:(i.IACentreName == r.IACentreName)
  action: ALLOW
}

rule IACVerifies{
  description:"IAC will verify"
  participant: "digitracker.network.IssuingAuthorityCentre"
  operation: ALL
  resource:"digitracker.network.VerifyApplication"
  action: ALLOW
}

rule IACValidates{
  description:"IAC will validate"
  participant: "digitracker.network.IssuingAuthorityCentre"
  operation: ALL
  resource:"digitracker.network.ValidateNextStage"
  action: ALLOW
}

rule IACIssues{
  description:"IAC will issue"
  participant: "digitracker.network.IssuingAuthorityCentre"
  operation: ALL
  resource:"digitracker.network.IssuePermit"
  action: ALLOW
}

rule IACRejectsApplication{
  description:"IAC will reject"
  participant: "digitracker.network.IssuingAuthorityCentre"
  operation: ALL
  resource:"digitracker.network.RejectApplication"
  action: ALLOW
}

/*===================FOR ENFORCEMENT_AUTHORITY=============*/
rule EnforcementAuthorityViewPermit{
  description: "EA can view a manipulate issued permit"
  participant(ea): "digitracker.network.EnforcingAuthority"
  operation: ALL
  resource(r):"digitracker.network.UserPermit"
  condition:(r.stage=="ISSUED"||r.stage=="SUSPENDED"||r.stage=="REVOKED")
  action: ALLOW
}

rule EASuspends{
  description:"EA will suspend"
  participant:"digitracker.network.EnforcingAuthority"
  operation: ALL
  resource:"digitracker.network.SuspendPermit"
  action: ALLOW
}
rule EARevokes{  
  description:"EA will revoke"
  participant:"digitracker.network.EnforcingAuthority"
  operation: ALL
  resource:"digitracker.network.RevokePermit"
  action: ALLOW
}

rule EARegistersViolation{  
  description:"EA will record violation"
  participant:"digitracker.network.EnforcingAuthority"
  operation: ALL
  resource:"digitracker.network.RegisterPenalty"
  action: ALLOW
}