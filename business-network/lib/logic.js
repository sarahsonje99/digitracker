'use strict';

// /**
//  * @param {digitracker.network.Pay} tx
//  * @returns{String}
//  * @transaction
//  */
// async function Pay(tx, amount){
//     let userWalletRegistry = await getAssetRegistry('digitracker.network.UserWallet');  
//     const userWallet = userWalletRegistry.get(tx.uId);
//     amountRequested = amount;
//     if (userWallet.balance<tx.amountRequested)
//         throw new Error("Not enough Balance");
//     balance -= amountRequested;
//     await userWalletRegistry.update(userWallet)
// }

/**
 * @param {digitracker.network.GetApplicationForm} tx
 * @returns{String}
 * @transaction
 */
async function GetApplicationForm(tx) {
    let aadhaarId = tx.aadhaarId;
    let permitName = tx.permitName;

    const permitMetadataRegistry = await getAssetRegistry('digitracker.network.PermitMetadata');
    const permitMetadata = await permitMetadataRegistry.get(permitName);

    const userPermits = await query('selectAllCitizenPermits', {aadhaar: tx.aadhaarId})
    
    let permitsFound = {}
    let flag = false;
    for (permitReq of permitMetadata.requiredPermitNames) {
        flag = false;
        for (userPermit of userPermits) {
            if(userPermit.permitName==permitReq && userPermit.stage=="ISSUED") {
                permitsFound[userPermit.permitName] = true;
                flag = true;
                break;
            }
        }
        if(flag==false)
            permitsFound[permitReq] = false;
    }
    
    const citizenRegistry = await getParticipantRegistry('digitracker.network.Citizen');
    const citizen = await citizenRegistry.get(aadhaarId);
    const citizenDocs = Object.keys(JSON.parse(citizen.docsJSON));
    let docsFound = {}
    flag = false;
    for (docReq of permitMetadata.requiredDocs) {
        flag = false
        for (citizenDoc of citizenDocs) {
            if(citizenDoc==docReq) {
                docsFound[docReq] = true;
                flag = true;
                break;
            }
        }
        if(flag==false)
            docsFound[docReq] = false;
    }

    const issuingAuthorityRegistry = await getParticipantRegistry('digitracker.network.IssuingAuthority');
    const IA = await issuingAuthorityRegistry.get(permitMetadata.IAName);
    const IACentreNames = IA.IACentreNames;

    let userPermitMetadata = {};
    userPermitMetadata["docsFound"] = JSON.stringify(docsFound);
    userPermitMetadata["permitsFound"] = JSON.stringify(permitsFound);
    userPermitMetadata["permitMetadata"] = JSON.stringify(permitMetadata);
    userPermitMetadata["IACentreNames"] = IACentreNames;

    return JSON.stringify(userPermitMetadata);
}

/**
 * @param {digitracker.network.SubmitApplication} tx
 * @transaction
 * 
 */
async function SubmitApplication(tx) {
    let today = new Date();

    let aadhaarId = tx.aadhaarId;
    let permitName = tx.permitName;

    let factory = getFactory();
    let permit = factory.newResource('digitracker.network', 'UserPermit', permitName+"#"+aadhaarId);
    permit.permitName = permitName;
    permit.aadhaarId = aadhaarId;
    permit.formJSON = tx.formJSON;
    permit.appliedDate = today;
    permit.IACentreName = tx.IACentreName;
    permit.violationList = [];
    permit.violationFee = [];
    permit.hasPaidCharge = [];

    const permitMetadataRegistry = await getAssetRegistry('digitracker.network.PermitMetadata');
    const permitMetadata = await permitMetadataRegistry.get(permitName);
    const validationReqs = permitMetadata.validationRequirements;
    const validationSize = validationReqs.length;
    const citizenRegistry = await getParticipantRegistry('digitracker.network.Citizen');
    let citizen = await citizenRegistry.get(aadhaarId);
    
    let docsJSON = JSON.parse(citizen.docsJSON);
    // Object.keys(jsonArray).length;
    let submittedDocsJSON = JSON.parse(tx.submittedDocsJSON);
    for(submittedDocName in submittedDocsJSON) {
        docsJSON[submittedDocName] = submittedDocsJSON[submittedDocName];
    }
    // return docsJSON;
    citizen.docsJSON = JSON.stringify(docsJSON);
    await citizenRegistry.update(citizen);
    
    let reqDocsJSON = {};
    let reqDocsList = permitMetadata.requiredDocs;
    for(doc of reqDocsList)
    {
        reqDocsJSON[doc] = docsJSON[doc];
    }
    permit.requiredDocsJSON = JSON.stringify(reqDocsJSON);
    
    permit.requiredDocsJSON = "{}";
    let validationJSON = {};
    let validationDatesJSON = {};
    for(let i = 0; i < validationSize; ++i) {
        validationJSON[validationReqs[i]] = false;
        validationDatesJSON[validationReqs[i]] = "";
    }
    permit.validationRequirementsJSON = JSON.stringify(validationJSON);
    permit.validationDatesJSON = JSON.stringify(validationDatesJSON);
    
    // // payment procedure
    // try {
    // throw new Error('before everything');
    // const userWalletRegistry = await getAssetRegistry('digitracker.network.UserWallet');
    // throw new Error('before user waller get statement');
    // const userWallet = await userWalletRegistry.get(aadharId);
    // throw new Error('before IAC wallet get statement');
    // const IACWallet = await userWalletRegistry.get(tx.IACentreName);
    // throw new Error('before let amount statement');
    // let amount = permitMetadata.applicationFee;
    // if(userWallet.balance<amount)
    //     throw new Error("Insufficient balance, please top up!");
    // userWallet.balance -= amount;
    // IACWallet.balance += amount;
    // permit.stage = "APPLIED";
    // permit.hasPaidAppFee = true;

    // await userWalletRegistry.update(userWallet);
    // await userWalletRegistry.update(IACWallet);
    // } catch(err) {
    //     throw new Error(err.message);
    // }

    permit.stage = "APPLIED";
    permit.hasPaidAppFee = true;

    const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    await userPermitRegistry.add(permit);

    //events
    let event = getFactory().newEvent('digitracker.network', 'ApplicationSubmitted');
    event.permitId = permit.permitId;
    event.aadhaarId = permit.aadhaarId;
    event.IACentreName = permit.IACentreName;
    event.type = "APPLICATION";
    emit(event);
}

/**
 * @param {digitracker.network.VerifyApplication} tx
 * @transaction
 */
async function VerifyApplication(tx) {
    const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    const permit = await userPermitRegistry.get(tx.permitName+"#"+tx.aadhaarId);

    const currentStage = permit.stage;
    if(currentStage != "APPLIED") {
        throw new Error("Form not in application stage");
    }
    
    permit.stage = "VERIFIED";

    await userPermitRegistry.update(permit);

    let event = getFactory().newEvent('digitracker.network', 'ApplicationVerified');
    event.permitId = permit.permitId;
    event.aadhaarId = permit.aadhaarId;
    event.IACentreName = permit.IACentreName;
    event.type = "VERIFICATION";
    emit(event);
}

/**
 * @param {digitracker.network.UpdateNlpScore} tx
 * @transaction
 */
async function UpdateNlpScore(tx) {
    const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    const permit = await userPermitRegistry.get(tx.permitName+"#"+tx.aadhaarId);

    const nlpScore = tx.nlpScore;
    permit.nlpScore = nlpScore;
    const validationRequirementsJSON = JSON.parse(permit.validationRequirementsJSON);

    if(nlpScore > 0.65) {
        permit.psychEval = true;
        validationRequirementsJSON['psychEval'] = false;
        permit.validationRequirementsJSON = JSON.stringify(validationRequirementsJSON);
    }

    await userPermitRegistry.update(permit);
}

/**
 * @param {digitracker.network.PsychEvaluation} tx
 * @transaction
 */
async function PsychEvaluation(tx) {
    const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    const permit = await userPermitRegistry.get(tx.permitName+"#"+tx.aadhaarId);

    permit.doctorCertificate = tx.doctorCertificate;

    await userPermitRegistry.update(permit);
}

/**
 * @param {digitracker.network.ValidateNextStage} tx
 * @transaction
 */
async function ValidateNextStage(tx) {
    const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    const permit = await userPermitRegistry.get(tx.permitName+"#"+tx.aadhaarId);

    const currentstage = permit.stage;

    const permitMetadataRegistry = await getAssetRegistry('digitracker.network.PermitMetadata');
    const permitMetadata = await permitMetadataRegistry.get(permit.permitName);

    if(currentstage != "VERIFIED") {
        throw new Error('Form is not in correct stage');
    }

    permit.validationRequirementsJSON = tx.validationRequirementsJSON;
    permit.validationDatesJSON = tx.validationDatesJSON;
    let localValidationRequirements = JSON.parse(permit.validationRequirementsJSON);
    for (key of Object.keys(localValidationRequirements)) {
        if (localValidationRequirements[key] == false) {
            permit.stage = currentstage;
            break;
        }
        else {
            permit.stage = "VALIDATED";
        }
    }
    await userPermitRegistry.update(permit);

    let event = getFactory().newEvent('digitracker.network', 'ApplicationValidated');
    event.permitId = permit.permitId;
    event.aadhaarId = permit.aadhaarId;
    event.IACentreName = permit.IACentreName;
    event.type = "VALIDATION";
    emit(event);
}

/**
 * @param {digitracker.network.IssuePermit} tx
 * @transaction
 */
async function IssuePermit (tx){
    const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    const permit = await userPermitRegistry.get(tx.permitName+"#"+tx.aadhaarId);

    const currentstage = permit.stage;
    // throw new Error(currentstage);
    const permitMetadataRegistry = await getAssetRegistry('digitracker.network.PermitMetadata');
    const permitMetadata = await permitMetadataRegistry.get(permit.permitName);
   

    if (permitMetadata.validationRequired==true && currentstage!="VALIDATED") {
        throw new Error("Cannot ISSUE application; complete previous stages first");
    }
    
    const validityPeriod = Number(permitMetadata.validityPeriod);
    
    //set stage 
    permit.stage = "ISSUED";

    // Set validTo as validFrom + validityPeriod months
    let currentDate = new Date();
    permit.validFrom = new Date();    //  07-06-2016; (Don't touch this line!)
    currentDate.setMonth( currentDate.getMonth() + validityPeriod );
    permit.validTill = currentDate;
    permit.permitNumber = (+currentDate).toString(36);
    await userPermitRegistry.update(permit);

    let event = getFactory().newEvent('digitracker.network', 'PermitIssued');
    event.permitId = permit.permitId;
    event.aadhaarId = permit.aadhaarId;
    event.IACentreName = permit.IACentreName;
    event.type = "ISSUAL";
    emit(event);
}

/**
 * @param {digitracker.network.RejectApplication} tx
 * @transaction
 */
async function RejectApplication (tx){
    const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    const permit = await userPermitRegistry.get(tx.permitName+"#"+tx.aadhaarId);

    permit.stage = tx.rejectionStage;
    permit.rejectedBecause = tx.rejectedBecause;
    permit.isRejected = true;
    await userPermitRegistry.update(permit);

    let event = getFactory().newEvent('digitracker.network', 'ApplicationRejected');
    event.permitId = permit.permitId;
    event.aadhaarId = permit.aadhaarId;
    event.rejectedBecause = tx.rejectedBecause;
    event.type = "REJECTION";
    emit(event);
}

/**
 * @param {digitracker.network.RevokePermit} tx
 * @transaction
 */
async function RevokePermit (tx){
    const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    // const permit = await userPermitRegistry.get(tx.permitName+"#"+tx.aadhaarId);
    const permitList = await query('selectPermitWithPermitNumber', {permitNumber: tx.permitNumber})
    let userPermit = permitList[0];
    userPermit.stage = "REVOKED";

    await userPermitRegistry.update(userPermit);

    let event = getFactory().newEvent('digitracker.network', 'PermitRevoked');
    event.permitId = userPermit.permitId;
    event.aadhaarId = userPermit.aadhaarId;
    event.date = tx.date;
    event.revokedBecause = tx.revokedBecause;
    event.type = "REVOCATION";
    emit(event);
}

/**
 * @param {digitracker.network.SuspendPermit} tx
 * @transaction
 */
async function SuspendPermit (tx){
    const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    // const permit = await userPermitRegistry.get(tx.permitName+"#"+tx.aadhaarId);
    const permitList = await query('selectPermitWithPermitNumber', {permitNumber: tx.permitNumber})
    let userPermit = permitList[0];
    userPermit.stage = "SUSPENDED";

    await userPermitRegistry.update(userPermit);

    let event = getFactory().newEvent('digitracker.network', 'PermitSuspended');
    event.permitId = userPermit.permitId;
    event.aadhaarId = userPermit.aadhaarId;
    event.date = tx.suspendTill;
    event.suspendedBecause = tx.suspendedBecause;
    event.suspendTill = tx.suspendTill;
    event.type = "SUSPENSION";
    emit(event);
}

/**
 * @param {digitracker.network.RegisterPenalty} tx
 * @transaction
 */
async function RegisterPenalty(tx) {
    const permitList = await query('selectPermitWithPermitNumber', {permitNumber: tx.permitNumber})

    // const permitMetadataRegistry = await getAssetRegistry('digitracker.network.PermitMetadata');
    // const permitMetadata = await permitMetadataRegistry.get(permit.permitName);
    let userPermit = permitList[0];
    if(userPermit.stage == "REVOKED" || userPermit.stage == "SUSPENDED" || userPermit.stage == "REJECTED") {
        throw new Error('Permit is not active.');
    }
    
    let userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    // let permit = await userPermitRegistry.get(userPermit.permitId);

    userPermit.violationList.push(tx.description);
    userPermit.violationFee.push(tx.penalty);
    userPermit.hasPaidCharge.push(false);

    await userPermitRegistry.update(userPermit);

    let event = getFactory().newEvent('digitracker.network', 'PenaltyRegistered');
    event.permitId = userPermit.permitId;
    event.penalty = tx.penalty;
    event.description = tx.description;
    event.date = tx.date;
    event.aadhaarId = userPermit.aadhaarId;
    event.type = "VIOLATION";
    emit(event);
}

/**
 * @param {digitracker.network.PayForViolation} tx
 * @transaction
 */
async function PayForViolation (tx){
    const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    const permit = await userPermitRegistry.get(tx.permitName+"#"+tx.aadhaarId);

    permit.hasPaidCharge[tx.violationArrayIndex] = true;

    await userPermitRegistry.update(permit);
}

// /**
//  * @param {digitracker.network.PayFine} tx
//  * @transaction
//  */
// async function PayFine (tx){
//     const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
//     const permit = await userPermitRegistry.get(tx.permitName+"#"+tx.aadhaarId);
//     const WalletRegistry = await getAssetRegistry('digitracker.network.UserWallet');
//     const userWallet = await WalletRegistry.get(tx.uId);
//     const IACWallet = await WalletRegistry.get(tx.IACentreName);

//     for (let i = 0; i<permit.hasPaidCharge.length; i++)
//     {
//         if(permit.hasPaidCharge[i]==false){
//             tx.amountRequested += permit.violationFee[i];
//         }
//     }

//     if(userWallet.balance<tx.amountRequested)
//         throw new Error("Insuffecient balance to settle all fines. Pay all fines together by refilling wallet.");
    
//     userWallet.balance -= tx.amountRequested;
//     RTOWallet.balance += tx.amountRequested;
    
//     for (let i = 0; i<permit.hasPaidCharge.length; i++)
//         permit.hasPaidCharge[i]=true;
    
//     await userPermitRegistry.update(permit);
//     await userWalletRegistry.update(userWallet);
//     await userWalletRegistry.update(IACWallet);
// }

/**
 * @param {digitracker.network.GetPermitHistory} tx
 * @returns{String}
 * @transaction
 */
async function GetPermitHistory(tx) {
    let aadhaarId = tx.aadhaarId;
    let permitName = tx.permitName;
    let permitHistory = {};

    const permitMetadataRegistry = await getAssetRegistry('digitracker.network.PermitMetadata');
    const permitMetadata = await permitMetadataRegistry.get(permitName);

    const userPermitRegistry = await getAssetRegistry('digitracker.network.UserPermit');
    const permit = await userPermitRegistry.get(permitName+"#"+aadhaarId);

    const citizenRegistry = await getParticipantRegistry('digitracker.network.Citizen');
    const citizen = await citizenRegistry.get(aadhaarId);

    // let historian = await getHistorian();
    
    permitHistory["IACentreName"] = permit.IACentreName;
    let history = [];
    let stageJSON = {};


    //APPLIED
    stageJSON["title"] = "APPLICATION";
    const appliedResults = await query('selectAppliedStage');
    let datetime = "";
    for(app of appliedResults) {
        // return app.eventsEmitted[0].aa;
        if(app.eventsEmitted[0].aadhaarId==aadhaarId)
            datetime = app.transactionTimestamp;
    }
    stageJSON["date"] = datetime.toISOString().substr(0, 10);

    let metadata = [];
    let meta = {};
    if(permitMetadata.requiredDocs!="") {
        meta["key"] = "Submitted Documents";
        meta["value"] = permitMetadata.requiredDocs;
        metadata.push(meta);
    }
    if(permitMetadata.requiredPermitNames!="") {
        meta = {};
        meta["key"] = "Linked Permits";
        meta["value"] = permitMetadata.requiredPermitNames;
        metadata.push(meta);
    }
    meta = {};
    meta["key"] = "Application fee";
    meta["value"] = permitMetadata.applicationFee;
    metadata.push(meta);
    stageJSON["metaInfo"] = metadata;
    history.push(stageJSON);

    //VERIFIED or REJECTED
    if(permit.stage=="APPLIED") {
        permitHistory["eachStage"] = history;
        return JSON.stringify(permitHistory); 
    }
    stageJSON = {};
    stageJSON["title"] = "VERIFICATION";
    const verifiedResults = await query('selectVerifiedStage');
    for(app of verifiedResults) {
        if(app.eventsEmitted[0].aadhaarId==aadhaarId)
            datetime = app.transactionTimestamp;
    }
    stageJSON["date"] = datetime.toISOString().substr(0, 10);
    metadata = [];
    if(permit.isRejected==true && permit.stage=="VERIFIED") { //Rejected at verification
        meta = {};
        meta["key"] = "Application Rejected";
        meta["value"] = "";
        metadata.push(meta);
        meta = {};
        meta["key"] = "Rejected Because";
        meta["value"] = permit.rejectedBecause;
        metadata.push(meta);

        stageJSON["metaInfo"] = metadata;
        history.push(stageJSON);
        permitHistory["eachStage"] = history;
        return JSON.stringify(permitHistory); 
    }
    else {        
        meta = {};
        meta["key"] = "Verified by";
        meta["value"] = permit.IACentreName;
        metadata.push(meta);
        stageJSON["metaInfo"] = metadata;
        history.push(stageJSON);
    }
    stageJSON={};
    metadata=[];
    //VALIDATION OR REJECTION
    if(permitMetadata.validationRequired==true) {
        let val = false;
        let vals = JSON.parse(permit.validationRequirementsJSON);
        for(done of Object.keys(vals)) {
            if(vals[done]==true)
                val = true;
        }
        if(val==false) {
            permitHistory["eachStage"] = history;
            return JSON.stringify(permitHistory); 
        }

        // return "hey";

        stageJSON["title"] = "VALIDATION";
        const validatedResults = await query('selectValidateStage');
        for(app of validatedResults) {
            if(app.eventsEmitted[0].aadhaarId==aadhaarId)
                datetime = app.transactionTimestamp;
        }
        stageJSON["date"] = datetime.toISOString().substr(0, 10);

        valDatesJSON = JSON.parse(permit.validationDatesJSON);
        meta = {};
        for(val of Object.keys(valDatesJSON))
        {
            meta = {};
            meta["key"] = val;
            if( valDatesJSON[val]=="")
                meta["value"] = "Not yet Validated"
            else
                meta["value"] = valDatesJSON[val];
            metadata.push(meta);
        }   
        stageJSON["metaInfo"] = metadata;
        history.push(stageJSON);
    }
    stageJSON={};
    metadata=[];

    //ISSUAL DETAILS
    if(permit.stage=="ISSUED" || permit.stage=="SUSPENDED" || permit.stage=="REVOKED") {
        stageJSON = {};
        stageJSON["title"] = "ISSUAL";
        if(permit.isRejected==true) {
            meta = {};
            meta["key"] = "Permit Application Rejected";
            meta["value"] = "";
            metadata.push(meta);
            meta = {};
            meta["key"] = permit.rejectedBecause;
            meta["value"] = "";
            metadata.push(meta);

            stageJSON["metaInfo"] = metadata;
            history.push(stageJSON);
            permitHistory["eachStage"] = history;
            return JSON.stringify(permitHistory);
        }
        
        const issuedResults = await query('selectIssuedStage');
        for(app of issuedResults) {
            if(app.eventsEmitted[0].aadhaarId==aadhaarId)
                datetime = app.transactionTimestamp;
        }
        stageJSON["date"] = datetime.toISOString().substr(0, 10);

        meta = {};
        meta["key"] = "Issued by";
        meta["value"] = permit.IACentreName;
        metadata.push(meta);
        meta = {};
        meta["key"] = "Permit ID number";
        meta["value"] = permit.permitNumber;
        metadata.push(meta);
        meta = {};
        meta["key"] = "Valid till";
        meta["value"] = permit.validTill;
        metadata.push(meta);

        stageJSON["metaInfo"] = metadata;
        history.push(stageJSON);
    }
    stageJSON={};
    metadata=[];

    if(permit.stage=="SUSPENDED") {
        stageJSON["title"] = "SUSPENSION";
        const suspendedResults = await query('selectSuspendedStage');
        for(app of suspendedResults) {
            if(app.eventsEmitted[0].aadhaarId==aadhaarId)
                datetime = app.transactionTimestamp;
        }
        stageJSON["date"] = datetime.toISOString().substr(0, 10);

        metadata = [];
        meta = {};
        meta["key"] = "Permit suspended beacause";
        meta["value"] = app.eventsEmitted[0].suspendedBecause;
        metadata.push(meta);
        meta = {};
        meta["key"] = "Suspended till";
        meta["value"] = app.eventsEmitted[0].suspendTill;
        metadata.push(meta);

        stageJSON["metaInfo"] = metadata;
        history.push(stageJSON);
    }
    stageJSON={};
    metadata=[];
    if(permit.stage=="REVOKED") {
        stageJSON["title"] = "REVOCATION";
        const revokedResults = await query('selectRevokedStage');
        for(app of revokedResults) {
            if(app.eventsEmitted[0].aadhaarId==aadhaarId)
                datetime = app.transactionTimestamp;
        }
        stageJSON["date"] = datetime.toISOString().substr(0, 10);

        let metadata = [];
        let meta = {};
        meta["key"] = "Permit revoked beacause";
        meta["value"] = app.eventsEmitted[0].revokedBecause;
        metadata.push(meta);
        stageJSON["metaInfo"] = metadata;
        history.push(stageJSON);
    }
    permitHistory["eachStage"] = history;
   
    return JSON.stringify(permitHistory);
}