const mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/digitracker');
const Schema = mongoose.Schema;

const UserDetail = new Schema({
    mobile: String,
    aadhaar: String
});

const UserIA = mongoose.Schema({
    username: String,
    ianame: String,
    password: String
});

const UserIACenter = mongoose.Schema({
    username: String,
    iacentername: String,
    password: String
});

// const EventSchema = new Schema({
//     eventJson: String,
//     aadhaarId: String,
//     IACentreName: {
//         type: String,
//         default: "-1",
//     },
// });

const UserDetails = mongoose.model('userInfo', UserDetail, 'userInfo');
const UserIAs = mongoose.model('userIa', UserIA, 'userIa');
const UserIACenters = mongoose.model('userIaCenter', UserIACenter, 'userIaCenter');
// const Event = mongoose.model('Event', EventSchema);
const conn = mongoose.connection;

const jay = {
    aadhaar: "73b665348fa91358d46fe30d51cf1f4d",
    mobile: "918655317088",
};

const insert = async () => {
    await UserDetails.remove();
    await UserIAs.remove();
    await UserIACenters.remove();
    // console.log(await UserDetails.find());

    await UserDetails(jay).save();
    await UserDetails({
        aadhaar: "b2e1267417f8c5665bf8b0db9f66d619",
        mobile: "7045152555",
    }).save();

    await UserDetails({
        aadhaar: "0f2fc10dee5435496c20f3fb95b9354f",
        mobile: "9869837671",
    }).save();

    await UserIAs({
        ianame: "RTO",
        username: "rto",
        password: "5f4dcc3b5aa765d61d8327deb882cf99"
    }).save();
    await UserIAs({
        ianame: "Passport Authority",
        username: "passport",
        password: "5f4dcc3b5aa765d61d8327deb882cf99"
    }).save();

    await UserIACenters({
        username: "wadala",
        iacentername: "Wadala RTO",
        password: "5f4dcc3b5aa765d61d8327deb882cf99",
    }).save();
    await UserIACenters({
        username: "thane",
        iacentername: "Thane RTO",
        password: "5f4dcc3b5aa765d61d8327deb882cf99",
    }).save();
    await UserIACenters({
        username: "kolkata",
        iacentername: "Kolkata RTO",
        password: "5f4dcc3b5aa765d61d8327deb882cf99",
    }).save();

    await UserIACenters({
        username: "mumbai",
        iacentername: "Mumbai PA",
        password: "5f4dcc3b5aa765d61d8327deb882cf99",
    }).save();
    await UserIACenters({
        username: "kolkatapa",
        iacentername: "Kolkata PA",
        password: "5f4dcc3b5aa765d61d8327deb882cf99",
    }).save();
    await UserIACenters({
        username: "delhi",
        iacentername: "Delhi PA",
        password: "5f4dcc3b5aa765d61d8327deb882cf99",
    }).save();


    // console.log(await JSON.stringify(await UserIACenters.find()));
};
insert();

module.exports = {
    UserDetails,
    UserIAs,
    UserIACenters,
    conn,
};