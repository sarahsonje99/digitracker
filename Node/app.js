const express = require('express'); 
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const paypal = require('paypal-rest-sdk');
const nodemailer = require('nodemailer');
const request = require('request');
const WebSocket = require('ws');
var crypto = require('crypto'); 
const session = require('express-session');
const cookieParser = require('cookie-parser');
const { sendMail } = require('./emailer');
const { pollExpiry } = require('./pollExpiry');
const { PythonShell } = require('python-shell');
var router = express.Router();

const {UserDetails, UserIACenters, UserIAs, conn} = require('./models/models');

const multer = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;
const gfs = Grid(conn.db);
const port = 4100;
const Nexmo = require('nexmo');
const nexmo = new Nexmo({
    apiKey: 'b3a3086f',
    apiSecret: 'C19edvHJcvrYB7f6'
});

const querystring = require('querystring');
const http = require('http');
// const http = require('http');
const fs = require('fs');
const io = require('socket.io')(http);


// var options = {
//     key: fs.readFileSync('example.com.key'),
//     cert: fs.readFileSync('example.com.crt')
//   };  

http.createServer(app).listen(port, function() {
    console.log('Server listening on port ' + port);
});

// console.log("before runpython");
// const nlpNumber = runPythonScript('../ML/twitterAnalysis.py');
// console.log("nlpNumber: " + nlpNumber);

pollExpiry();
setTimeout(pollExpiry, 1000 * 60 * 60 * 24);

/*  Web Socket listening to composer-rest-server */
const url = 'ws://localhost:3000';
const connection = new WebSocket(url);

connection.on('open', () => {
    console.log('Connected to rest-server socket');
});

connection.on('error', error => {
    console.log(`WebSocket error: ${error}`);
});

connection.on('message', async (message) => {
    const event = JSON.parse(message);
    console.log(event.permitId);
    const url = "http://localhost:3000/api/Citizen/" + event.aadhaarId;
    const options = {};
    options.url = url;
    options.method = 'GET';
    options.headers = {};
    options.headers['accept'] = 'application/json';
    request(options, async (err, response, body) => {
        const citizen = JSON.parse(body);
        if(err) {
            throw err;
        }
        const emailId = citizen.emailId;
        const permitName = event.permitId.split("#")[0];

        const subject = "Update regarding your " + permitName;
        const text = event.type;
        sendMail(emailId, subject, text);
    });

});
/*============== WEB SOCKET END ====================================  */

/*========================SOCKET IO START==============*/
io.on('connection', function(socket){
    console.log('a user connected');
    socket.emit('message', {data: "yo"});
  });
/*==============================SOCKET IO END=============*/

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Credentials', true);
    next();
});
app.use(cookieParser());

// Allows cross-origin domains to access this API
app.use((req, res, next) => {
    res.append("Access-Control-Allow-Headers", "Origin, Accept,Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    next();
});

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

// BodyParser middleware
app.use(bodyParser.json());

app.use(session({ secret: 'this-is-a-secret-token', cookie: { maxAge: Date.now() + (30 * 86400 * 1000) }, resave: true, saveUninitialized: true}));


/* ------------------------------------ CONFIGURATION START ------------------------------------ */

// Paypal configuration
paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': 'AV7wZp-Z3iQp0271jkJwrOO1DB-6t3w8vCfZeZF_Y6Q54yJYus3SnPOWTDai0sBdlqrKLYMFv580pE5o', // Client ID cam be found in the developers console of paypal
    'client_secret': 'EJWQNPtxm8rhY14usiZb9Vk3u7iNnqMdldFJbqRmw633bXu8QKbK2irGIemFq5brfFcSFwmKbsimiMNP' // Client secret cam be found in the developers console of paypal
});

// Setting up the root route
app.get('/', (req, res) => {
    res.send('Welcome to the express server');
});

/* -------------------------------------- CONFIGURATION END -------------------------------------- */


/* --------------------------------------- USER AUTH START --------------------------------------- */


// LOGIN IA
app.post('/api/loginia', (req,res) => {
    const username = req.body.username;
    const ianame = req.body.ianame;
    console.log(JSON.stringify(req.body))
    var md5sum = crypto.createHash('md5');
    const password = md5sum.update(req.body.password).digest('hex');
    console.log(password);
    // const aadhaar = req.body.aadhaar;
    UserIAs.findOne({
        username: username
    }, function(err, user) {
        console.log(JSON.stringify(user));
        if (err) {
            console.log(err);
            res.json({ "auth": "false", "message:":"Error" });
        }
        else if (!user) {
            res.json({ "auth": "false", "message:":"No such user" });
        }
        else {
            if(user.ianame === ianame && user.password === password) {
                console.log('pass');
                req.session.Id = user.ianame;
                res.json({"auth": true, "message": "User login successful"});
            }
        }
    });
});


// LOGIN IACENTER
app.post('/api/loginiacenter', (req,res) => {
    console.log(JSON.stringify(req.session));
    if(req.session.Id === req.body.username) {
        res.json({
            "auth": true, "message": "You were already logged in!"}
        );
    }
    const username = req.body.username;
    const iacentername = req.body.iacentername;
    var md5sum = crypto.createHash('md5');
    const password = md5sum.update(req.body.password).digest('hex');
    console.log("loginiacenter: before findOne()");
    UserIACenters.findOne({
        username: username
    }, function(err, user) {
        console.log(JSON.stringify(user));
        if (err) {
            console.log(err);
            res.json({ "auth": "false", "message:":"Error" });
        }
        else if (!user) {
            res.json({ "auth": "false", "message:":"No such center" });
        }
        else {
            if(user.username === username && user.password === password) {
                req.session.Id = user.username;
                res.json({"auth": true, "message": "center login successful"});
            }
        }
    });
});


// LOGIN CITIZEN
app.post('/api/login', (req,res) => {
    console.log('Start:' + req.body.aadhaar);
    var md5sum = crypto.createHash('md5');
    const aadhaar = md5sum.update(req.body.aadhaar).digest('hex');
    // const aadhaar = req.body.aadhaar;
    console.log(aadhaar);
    UserDetails.findOne({
        aadhaar: aadhaar
    }, function(err, user) {
        console.log(JSON.stringify(user));
        if (err) {
            console.log(err);
            res.json({ "auth": "false", "message:":"Error" });
        }
        else if (!user) {
            res.json({ "auth": "false", "message:":"No such user" });
        }
        else {
            let phoneNumber = user.mobile;
            console.log(phoneNumber);
            nexmo.verify.request({number: '91'+phoneNumber, brand: 'DigiTracker'}, (err, result) => {
                if(err) {
                    res.sendStatus(500);
                } else {
                    let requestId = result.request_id;
                    if(result.status == '0') {
                        req.session.Id = user.aadhaar;
                        res.json({"mobile": phoneNumber, "requestId": requestId}); // Success! Now, have your user enter the PIN
                    } else {
                        res.status(401).send(result.error_text);
                    }
                }
            });
        }
    });
});

// REGISTER APIS

app.post('/api/register-mobile', (req, res) => {
    const mobile = req.body.mobile;

    UserDetails.findOne({
        mobile: mobile,
    }, function(err, user) {
        if (err) {
            console.log(err);
            res.json({ "auth": "false", "message:":"Error" });
        }
        if (!user) {
            res.json({ "auth": "true", "message:":"New User can be added" });
        }
    });
});

app.post('/api/register', (req, res) => {
    const $class = req.body.$class;
    const aadhaarId = req.body.aadhaarId;
    const dob = req.body.dob;
    const firstName = req.body.firstName;
    const middleName = req.body.middleName;
    const lastName = req.body.lastName;
    const phoneNo = req.body.phoneNo;
    const emailId = req.body.emailId;
    const docsJSON = req.body.docsJSON;
    var md5sum = crypto.createHash('md5');
    const aadhaar = md5sum.update(req.body.aadhaarId).digest('hex');
    // const password = md5sum.update(req.body.password).digest('hex');

    UserDetails.findOne({
        aadhaar: aadhaar
    }, function(err, user) {
        if(err)
            res.json({ "message": err });
        else if(user)
            res.json({ "auth": "false", "message": "Aadhaar card already exist!" });
        else {
            var new_user = new UserDetails({
                mobile: phoneNo,
                aadhaar: aadhaar,
            });

            new_user.save(function(error) {
                if(error)
                    res.json({ error});
                else {
                    console.log("USER ADDED SUCCESSFULLY");
                    req.session.Id = aadhaar;
                    res.json(true);
                }
            });
        }
    });

    const url = 'http://localhost:3000/api/Citizen';
    const options = {};
    options.url = url;
    options.method = 'POST',
    options.headers = {};
    options.headers['content-type'] = 'application/json';
    options.headers['accept'] = 'application/json';
    options.json = {
        "$class": $class,
        "aadhaarId": aadhaarId,
        "dob": dob,
        "firstName": firstName,
        "middleName": middleName,
        "lastName": lastName,
        "phoneNo": phoneNo,
        "emailId": emailId,
        docsJSON,
    };
    request(options, (err, response, body) => {
        if(err) {
            throw err;
        }
    });
});

app.post('/api/email-verify', (req, res) => {
    const email = req.body.email
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'digitrackerverify@gmail.com',
            pass: 'digitracker@6'
        }
    });
    let otp = Math.floor(1000 + Math.random() * 9000)   
    let mailOptions = {
        from: 'digitrackerverify@gmail.com',
        to: email,
        subject: 'OTP for e-mail verification',
        text: 'Your digitracker OTP is '+otp,
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
            res.json(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
        res.json(otp);
    });
    
});

/* ---------------------------------------- USER AUTH END ---------------------------------------- */


/* -------------------------------------- FILE UPLOAD START -------------------------------------- */
// Setting up the storage element
let storage = GridFsStorage({
    gfs : gfs,

    filename: (req, file, cb) => {
        let date = Date.now();
        // The way you want to store your file in database
        cb(null, file.fieldname + '-' + date + '.'); 
    },
    
    // Additional Meta-data that you want to store
    metadata: function(req, file, cb) {
        cb(null, { originalname: file.originalname });
    },
    root: 'file' // Root collection name
});

// Multer configuration for single file uploads
let upload = multer({
    storage: storage
}).single('file');

// Route for file upload
app.post('/api/upload', (req, res) => {
    upload(req,res, (err) => {
        if(err){
                res.json({error_code:1,err_desc:err});
                return;
        }
        res.json({error_code:0, error_desc: null, file_uploaded: true});
    });
});

// Downloading a single file
app.get('/api/file/:filename', (req, res) => {
    gfs.collection('file'); //set collection name to lookup into

    /** First check if file exists */
    gfs.files.find({filename: req.params.filename}).toArray(function(err, files){
        if(!files || files.length === 0){
            return res.status(404).json({
                responseCode: 1,
                responseMessage: "error"
            });
        }
        // create read stream
        var readstream = gfs.createReadStream({
            filename: files[0].filename,
            root: "file"
        });
        // set the proper content type 
        res.set('Content-Type', files[0].contentType)
        // Return response
        return readstream.pipe(res);
    });
});

// Route for getting all the files
app.get('/api/files', (req, res) => {
    let filesData = [];
    let count = 0;
    gfs.collection('file'); // set the collection to look up into

    gfs.files.find({}).toArray((err, files) => {
        // Error checking
        if(!files){
            return res.status(404).json({
                responseCode: 1,
                responseMessage: "error"
            });
        }
        // Loop through all the files and fetch the necessary information
        files.forEach((file) => {
            filesData[count++] = {
                originalname: file.metadata.originalname,
                filename: file.filename,
                contentType: file.contentType
            }
        });
        res.json(filesData);
    });
});
/* -------------------------------------- FILE UPLOAD END -------------------------------------- */


/* ----------------------------------------- OTP START ----------------------------------------- */


app.post('/api/otp', (req, res) => {
    let phoneNumber = req.body.number;
    console.log(phoneNumber);
    nexmo.verify.request({number: phoneNumber, brand: 'DigiTracker'}, (err, 
    result) => {
        if(err) {
            res.sendStatus(500);
        } else {
            console.log(JSON.stringify(result));
            let requestId = result.request_id;
            if(result.status == '0') {
                res.json({requestId: requestId}); // Success! Now, have your user enter the PIN
            } else {
                res.status(401).send(result.error_text);
            }
        }
    });
});

app.post('/api/verify-otp', (req, res) => {
    let pin = req.body.pin;
    let requestId = req.body.requestId;

    console.log(requestId)
    console.log(pin)

    nexmo.verify.check({request_id: requestId, code: pin}, (err, result) => {
        if(err) {
            // handle the error
            console.log(err);
        } else {
            if(result && result.status == '0') { // Success!
                res.json({"message": "Account verified!"});
            } else {
                // handle the error - e.g. wrong PIN
            }
        }
    });
});

/* ------------------------------------------ OTP END ------------------------------------------ */


/* ---------------------------------------- PAYPAL START --------------------------------------- */


var amount;

app.get('/api/pay/:price', (req, res) => {
    var url;
    const p = (req.params.price)/60;
    const price = Math.round(p*100)/100;
    amount = price;
    console.log(price)
    const create_payment_json = {
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": "http://localhost:4200/formren",
            "cancel_url": "http://localhost:4200/payment"
        },
        "transactions": [{
            "item_list": {
                "items": [{
                    "name": "Digitracker",
                    "sku": "001",
                    "price": price,
                    "currency": "USD",
                    "quantity": 1
                }]
            },
            "amount": {
                "currency": "USD",
                "total": price
            },
            "description": "DigiTracker Payment for Permit"
        }]
    };
    
    paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
            throw error;
        } else {
            for(let i=0;i<payment.links.length;i++){
                if(payment.links[i].rel === "approval_url"){
                    console.log("Payment:="+payment.links[i].href)
                    res.json({"url": payment.links[i].href})
                }
            }
        }
    });
});

app.post('/api/pay-success', (req, res) => {
    var price = amount;
    const payerId = req.body.PayerID;
    const paymentId = req.body.paymentId;

    const execute_payment_json = {
        "payer_id": payerId,
        "transactions": [{
            "amount": {
                "currency": "USD",
                "total": price
            }
        }]
    };

    paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
        if (error) {
            console.log(error.response);
            throw error;
        } else {
            console.log("Get Payment Response");
            console.log(JSON.stringify(payment));
            res.json(payment);
        }
    });
});

app.post('/api/:apiName', (req, res) => {
    const postData = req.body;
    const apiName = req.params.apiName;
    const composerApiURL = 'http://localhost:3000/api/';
    const options = {};
    options.url = composerApiURL + apiName;
    options.method = 'POST',
    options.headers = {};
    options.headers['content-type'] = 'application/json';
    options.headers['accept'] = 'application/json';
    options.json = postData;
    request(options, (err, response, body) => {
        if(err) {
            throw err;
        }
        res.json(body);
    });
    // res.status(500).end();
});

app.get('/api/:apiName/:id', (req, res) => {
    const apiName = req.params.apiName;
    const id = req.params.id;
    const composerApiURL = 'http://localhost:3000/api/';
    const options = {};
    options.url = composerApiURL + apiName + '/' + id;
    options.method = 'GET',
    options.headers = {};
    options.headers['accept'] = 'application/json';
    options.json = req.body;
    request(options, (err, response, body) => {
        if(err) {
            throw err;
        }
        res.json(body);
    });
    // res.status(500).end();
});

app.get('/api/:apiName/', (req, res) => {
    const apiName = req.params.apiName;
    const id = req.params.id;
    const composerApiURL = 'http://localhost:3000/api/';
    const options = {};
    options.url = composerApiURL + apiName;
    options.method = 'GET',
    options.headers = {};
    options.headers['accept'] = 'application/json';
    options.json = req.body;
    request(options, (err, response, body) => {
        if(err) {
            throw err;
        }
        res.json(body);
    });
    // res.status(500).end();
});
/* ----------------------------------------- PAYPAL END ---------------------------------------- */

app.get('/nlp-analysis', (req, res) => {
    let pyShell = new PythonShell('../ML/twitterAnalysis.py');
    console.log('inside nlp-analysis start');
    new Promise((resolve, reject) => {
        pyShell.on('message', message => {
            console.log(message);
            resolve(message);
        });
    }).then(answer => {
        res.status(200).send(answer);
    });
    console.log('end of nlp-analysis');
});

// app.listen(port, function () {
//     console.log('Server listening on port ' + port);
// });
