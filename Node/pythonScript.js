// const spawn = require('child_process').spawn;

// const pythonScript = async (path) => {
//     const pythonProcess = spawn('python', [path]);

//     pythonProcess.stdout.on('data', (data) => {
//         return data;
//     });
// };
const { PythonShell } = require('python-shell');

const runPythonScript = async (path) => {
    let pyShell = new PythonShell(path);
    console.log('inside runpythonScript');
    const message = await pyShell.on('message', message => {
        console.log(message);
        return message;
    });
};

module.exports = {
    runPythonScript,
};