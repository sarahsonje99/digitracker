const request = require('request');
const { sendMail } = require('./emailer');

const pollExpiry = async () => {
    const today = new Date(1572546600000); // 1st november
    const url = "http://localhost:3000/api/UserPermit";
    const options = {};
    options.url = url;
    options.method = 'GET';
    options.headers = {};
    options.headers['accept'] = 'application/json';
    request(options, async (err, response, body) => {
        // console.log(JSON.stringify(body));
        // console.log(body.length);
        userPermits = JSON.parse(body);
        if(err) {
            throw err;
        }
        for(let userPermit of userPermits) {
            request({
                url: "http://localhost:3000/api/Citizen/" + userPermit.aadhaarId,
                method: 'GET',
                headers: { 'accept': 'application/json' },
            }, async (err, response, body) => {
                const citizen = JSON.parse(body);
                const citizenEmail = citizen.emailId;
                const validTill = new Date(userPermit.validTill);
                const validTillYear = validTill.getFullYear();
                const currentYear = today.getFullYear();
                const validTillDate = validTill.getDate();
                const currentDate = today.getDate();
                // console.log(validTillYear + " " + currentYear);
                if(validTillYear === currentYear && validTillDate - currentDate <= 10) {
                    // console.log('sending renewal email to ' + citizenEmail);
                    const subject = "Your permit " + userPermit.permitName + " is expiring soon!";
                    const text = "Renew it on time!";
                    sendMail(citizenEmail, subject, text);
                }
            });
        }
    });
};

module.exports = {
    pollExpiry,
};