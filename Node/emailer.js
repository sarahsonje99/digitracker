const nodemailer = require('nodemailer');

const sendMail = async (receiverEmail, subject, text) => {
    console.log(receiverEmail + " " + subject + " " + text);
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'digitrackerverify@gmail.com',
            pass: 'digitracker@6',
        },
    });
    let mailOptions = {
        from: 'digitrackerverify@gmail.com',
        to: receiverEmail,
        subject,
        text,
    };
    transporter.sendMail(mailOptions, (err, info) => {
        if(err) {
            throw err;
        }
    });
};

module.exports = {
    sendMail,
};