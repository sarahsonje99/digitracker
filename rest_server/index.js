const express = require('express')
const app = express()

const fileUpload = require('express-fileupload');
var cors = require('cors');
var bodyParser = require('body-parser')
app.use( bodyParser.json() );
app.use(fileUpload());
app.use(cors({
    origin: ['*'],
  credentials: true,
}));



app.post('/api/login', function (req, res) {
  // ...
})
app.post('/api/ping', function (req, res) {
  // ...
})
app.post('/api/logout', function (req, res) {
  // ...
})